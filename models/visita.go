package models;

import(
    "fmt"
    "database/sql"
    "gopkg.in/kataras/iris.v6"
    "errors"
)

type Visita struct{
    Cod_visita_ocorrencia   int64   `json:"cod"`
    Cod_visita_plan         sql.NullInt64   `json:"codVisitaOcorrenciaPlan"`
    Dia_da_semana           int     `json:"diaDaSemana"`
    Cod_relatorio           int64   `json:"codRelatorio"`
    Cod_cli_forn_vend_sec   int64   `json:"pkCliFornVendSec"`
    Vlr_venda               float64 `json:"vlrVenda"`
    Obj_da_visita           int64   `json:"ObjVisita"`
    Desc_visita_realizada   sql.NullString  `json:"descVisitaRealizada"`
    Tipo                    string  `json:"tipo"`
    Data_registro           string  `json:"dataRegistro"`
}

func (t* Visita) Save(ctx *iris.Context)(error){      
    _,err := ctx.Get("db_vendas").(*sql.DB).
        Exec("INSERT INTO VISITA_OCORRENCIA (COD_VISITA_OCORRENCIA_PLAN,DIA_DA_SEMANA,COD_RELATORIO,COD_CLI_FORN_VEND_SEC,VLR_VENDA,OBJ_DA_VISITA,DESC_VISITA_REALIZADA,TIPO,DATA_REGISTRO) VALUES (?,?,?,?,?,?,?,?,?);",
        t.Cod_visita_plan,t.Dia_da_semana,t.Cod_relatorio,t.Cod_cli_forn_vend_sec,t.Vlr_venda,t.Obj_da_visita,t.Desc_visita_realizada,t.Tipo,t.Data_registro);
    if err != nil { return errors.New(err.Error())}
    return nil;
}

func (t *Visita) UpdateVisita(ctx *iris.Context)(error){
    _,err := ctx.Get("db_vendas").(*sql.DB).
        Exec("UPDATE VISITA_OCORRENCIA SET COD_VISITA_OCORRENCIA_PLAN = ?, DIA_DA_SEMANA = ?, COD_RELATORIO = ?, COD_CLI_FORN_VEND_SEC = ?, VLR_VENDA = ?,OBJ_DA_VISITA = ?,DESC_VISITA_REALIZADA = ?,TIPO = ?,DATA_REGISTRO = ? WHERE COD_VISITA_OCORRENCIA = ?;",
        t.Cod_visita_plan,t.Dia_da_semana,t.Cod_relatorio,t.Cod_cli_forn_vend_sec,t.Vlr_venda,t.Obj_da_visita,t.Desc_visita_realizada,t.Tipo,t.Data_registro,t.Cod_visita_ocorrencia);
    if err != nil { return errors.New(err.Error())}
    return nil;
}

func (t *Visita) GetVisitaNaoRealizadaDoRelatorio(ctx *iris.Context)([]Visita,error){
    result := []Visita{}
    //usando o Query junto com o ? evita SQL INJECTION
    rows,err := ctx.Get("db_vendas").(*sql.DB).
            Query("SELECT * FROM VISITAS_SEM_REALIZACAO WHERE COD_RELATORIO=?",t.Cod_relatorio);

    if err != nil { 
        fmt.Println("ERRO AQUI");
        return result,errors.New("Erro na query:"+err.Error());
    }    
    
    var dt_registro,tipo string    
    var obj_da_visita,cod_cli_forn_vend_sec,cod_relatorio,cod_visita_ocorrencia int64
    var cod_visita_plan sql.NullInt64
    var desc_vis_realizada sql.NullString

    var dia_da_semana int
    var valor float64

    for rows.Next(){			
        //fmt.Println("entrou")
        err := rows.Scan(&cod_visita_ocorrencia,&cod_visita_plan,&dia_da_semana,&cod_relatorio,&cod_cli_forn_vend_sec,&valor,&obj_da_visita,&desc_vis_realizada,&tipo,&dt_registro)
        if err != nil {
            return result, errors.New("Erro no Scan:"+err.Error())
        }
        result = append(result, Visita{
            Cod_visita_ocorrencia   : cod_visita_ocorrencia,
            Cod_visita_plan         : cod_visita_plan,
            Dia_da_semana           : dia_da_semana,
            Cod_relatorio           : cod_relatorio,
            Cod_cli_forn_vend_sec   : cod_cli_forn_vend_sec,
            Vlr_venda               : valor,
            Obj_da_visita           : obj_da_visita,
            Desc_visita_realizada   : desc_vis_realizada,
            Tipo                    : tipo,
            Data_registro           : dt_registro,
        })
    }
    return result, nil;
}

func (t *Visita) GetVisitaRealizadaFromRelatorio(ctx *iris.Context)([]Visita,error){
    result := []Visita{}
    //usando o Query junto com o ? evita SQL INJECTION
    rows,err := ctx.Get("db_vendas").(*sql.DB).
            Query("SELECT * FROM VISITA_OCORRENCIA WHERE COD_RELATORIO=? AND (TIPO='R' or TIPO='N')",t.Cod_relatorio);

    if err != nil {         
        return result,errors.New("Erro na query:"+err.Error());
    }    
    
    var dt_registro,tipo string    
    var obj_da_visita,cod_cli_forn_vend_sec,cod_relatorio,cod_visita_ocorrencia int64
    var cod_visita_plan sql.NullInt64
    var desc_vis_realizada sql.NullString

    var dia_da_semana int
    var valor float64

    for rows.Next(){			
        //fmt.Println("entrou")
        err := rows.Scan(&cod_visita_ocorrencia,&cod_visita_plan,&dia_da_semana,&cod_relatorio,&cod_cli_forn_vend_sec,&valor,&obj_da_visita,&desc_vis_realizada,&tipo,&dt_registro)
        if err != nil {
            return result, errors.New("Erro no Scan:"+err.Error())
        }
        
        result = append(result, Visita{
            Cod_visita_ocorrencia   : cod_visita_ocorrencia,
            Cod_visita_plan         : cod_visita_plan,
            Dia_da_semana           : dia_da_semana,
            Cod_relatorio           : cod_relatorio,
            Cod_cli_forn_vend_sec   : cod_cli_forn_vend_sec,
            Vlr_venda               : valor,
            Obj_da_visita           : obj_da_visita,
            Desc_visita_realizada   : desc_vis_realizada,
            Tipo                    : tipo,
            Data_registro           : dt_registro,
        })
    }
    return result, nil;
}

func (t *Visita) GetVisitaRealizadaDoDiaFromRelatorio(ctx *iris.Context)([]Visita,error){
    result := []Visita{}
    //usando o Query junto com o ? evita SQL INJECTION
    rows,err := ctx.Get("db_vendas").(*sql.DB).
            Query("SELECT * FROM VISITA_OCORRENCIA WHERE COD_RELATORIO=? AND (TIPO='R' or TIPO='N') AND DIA_DA_SEMANA=?",t.Cod_relatorio,t.Dia_da_semana);

    if err != nil {         
        return result,errors.New("Erro na query:"+err.Error());
    }    
    
    var dt_registro,tipo string    
    var obj_da_visita,cod_cli_forn_vend_sec,cod_relatorio,cod_visita_ocorrencia int64
    var cod_visita_plan sql.NullInt64
    var desc_vis_realizada sql.NullString

    var dia_da_semana int
    var valor float64

    for rows.Next(){			
        //fmt.Println("entrou")
        err := rows.Scan(&cod_visita_ocorrencia,&cod_visita_plan,&dia_da_semana,&cod_relatorio,&cod_cli_forn_vend_sec,&valor,&obj_da_visita,&desc_vis_realizada,&tipo,&dt_registro)
        if err != nil {
            return result, errors.New("Erro no Scan:"+err.Error())
        }
        
        result = append(result, Visita{
            Cod_visita_ocorrencia   : cod_visita_ocorrencia,
            Cod_visita_plan         : cod_visita_plan,
            Dia_da_semana           : dia_da_semana,
            Cod_relatorio           : cod_relatorio,
            Cod_cli_forn_vend_sec   : cod_cli_forn_vend_sec,
            Vlr_venda               : valor,
            Obj_da_visita           : obj_da_visita,
            Desc_visita_realizada   : desc_vis_realizada,
            Tipo                    : tipo,
            Data_registro           : dt_registro,
        })
    }
    return result, nil;
}

func (t *Visita) GetVisitaNaoRealizadaDoDiaByRelatorio(ctx *iris.Context)([]Visita,error){
    result := []Visita{}
    //usando o Query junto com o ? evita SQL INJECTION
    rows,err := ctx.Get("db_vendas").(*sql.DB).
            Query("SELECT * FROM VISITAS_SEM_REALIZACAO WHERE DIA_DA_SEMANA=? AND COD_RELATORIO=?",t.Dia_da_semana,t.Cod_relatorio);

    if err != nil { 
        fmt.Println("ERRO AQUI");
        return result,errors.New("Erro na query:"+err.Error());
    }    
    
    var dt_registro,tipo string    
    var obj_da_visita,cod_cli_forn_vend_sec,cod_relatorio,cod_visita_ocorrencia int64
    var cod_visita_plan sql.NullInt64
    var desc_vis_realizada sql.NullString

    var dia_da_semana int
    var valor float64

    for rows.Next(){			
        //fmt.Println("entrou")
        err := rows.Scan(&cod_visita_ocorrencia,&cod_visita_plan,&dia_da_semana,&cod_relatorio,&cod_cli_forn_vend_sec,&valor,&obj_da_visita,&desc_vis_realizada,&tipo,&dt_registro)
        if err != nil {
            return result, errors.New("Erro no Scan:"+err.Error())
        }
        
        result = append(result, Visita{
            Cod_visita_ocorrencia   : cod_visita_ocorrencia,
            Cod_visita_plan         : cod_visita_plan,
            Dia_da_semana           : dia_da_semana,
            Cod_relatorio           : cod_relatorio,
            Cod_cli_forn_vend_sec   : cod_cli_forn_vend_sec,
            Vlr_venda               : valor,
            Obj_da_visita           : obj_da_visita,
            Desc_visita_realizada   : desc_vis_realizada,
            Tipo                    : tipo,
            Data_registro           : dt_registro,
        })
    }
    return result, nil;
}


func (t *Visita) GetVisitaPlanejadaByDiaAndCodRelatorio(ctx *iris.Context)([]Visita,error){
    result := []Visita{}
    //usando o Query junto com o ? evita SQL INJECTION
    rows,err := ctx.Get("db_vendas").(*sql.DB).
            Query("SELECT * FROM VISITA_OCORRENCIA WHERE DIA_DA_SEMANA=? AND COD_RELATORIO=? AND TIPO='P'",t.Dia_da_semana,t.Cod_relatorio);

    if err != nil { 
        fmt.Println("ERRO AQUI");
        return result,errors.New("Erro na query:"+err.Error());
    }    
    
    var dt_registro,tipo string    
    var obj_da_visita,cod_cli_forn_vend_sec,cod_relatorio,cod_visita_ocorrencia int64
    var cod_visita_plan sql.NullInt64
    var desc_vis_realizada sql.NullString

    var dia_da_semana int
    var valor float64

    for rows.Next(){			
        //fmt.Println("entrou")
        err := rows.Scan(&cod_visita_ocorrencia,&cod_visita_plan,&dia_da_semana,&cod_relatorio,&cod_cli_forn_vend_sec,&valor,&obj_da_visita,&desc_vis_realizada,&tipo,&dt_registro)
        if err != nil {
            return result, errors.New("Erro no Scan:"+err.Error())
        }
        
        result = append(result, Visita{
            Cod_visita_ocorrencia   : cod_visita_ocorrencia,
            Cod_visita_plan         : cod_visita_plan,
            Dia_da_semana           : dia_da_semana,
            Cod_relatorio           : cod_relatorio,
            Cod_cli_forn_vend_sec   : cod_cli_forn_vend_sec,
            Vlr_venda               : valor,
            Obj_da_visita           : obj_da_visita,
            Desc_visita_realizada   : desc_vis_realizada,
            Tipo                    : tipo,
            Data_registro           : dt_registro,
        })
    }
    return result, nil;
}

func (t *Visita) GetAllVisitaFromRelatorio(ctx *iris.Context)([]Visita,error){
    result := []Visita{}
    //usando o Query junto com o ? evita SQL INJECTION
    rows,err := ctx.Get("db_vendas").(*sql.DB).
            Query("SELECT * FROM VISITA_OCORRENCIA WHERE COD_RELATORIO=? ",t.Cod_relatorio);

    if err != nil {         
        return result,errors.New("Erro na query:"+err.Error());
    }    
    
    var dt_registro,tipo string    
    var obj_da_visita,cod_cli_forn_vend_sec,cod_relatorio,cod_visita_ocorrencia int64
    var cod_visita_plan sql.NullInt64
    var desc_vis_realizada sql.NullString

    var dia_da_semana int
    var valor float64

    for rows.Next(){			
        //fmt.Println("entrou")
        err := rows.Scan(&cod_visita_ocorrencia,&cod_visita_plan,&dia_da_semana,&cod_relatorio,&cod_cli_forn_vend_sec,&valor,&obj_da_visita,&desc_vis_realizada,&tipo,&dt_registro)
        if err != nil {
            return result, errors.New("Erro no Scan:"+err.Error())
        }
        
        result = append(result, Visita{
            Cod_visita_ocorrencia   : cod_visita_ocorrencia,
            Cod_visita_plan         : cod_visita_plan,
            Dia_da_semana           : dia_da_semana,
            Cod_relatorio           : cod_relatorio,
            Cod_cli_forn_vend_sec   : cod_cli_forn_vend_sec,
            Vlr_venda               : valor,
            Obj_da_visita           : obj_da_visita,
            Desc_visita_realizada   : desc_vis_realizada,
            Tipo                    : tipo,
            Data_registro           : dt_registro,
        })
    }
    return result, nil;
}

