package models;

type VisitaRelatorio struct {
    Cliente string `json:"nome"`
    Objetivo string `json:"objetivo"`
    Vendas float64 `json:"vendas"`    
}

type LinhaVisita struct{
    Planejado VisitaRelatorio `json:"plan"`
    Realizado VisitaRelatorio `json:"real"`
}

type LinhaVisitaList []LinhaVisita

type VisitaRelatorioJSON struct{
    Visita []LinhaVisitaList `json:"visitas"`
}