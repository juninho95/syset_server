package models;

import(
    _ "fmt"
    "database/sql"
    "gopkg.in/kataras/iris.v6"
    "errors"
)

type VendedorFornecedor struct{
    Id              int64 `json:"id"`
    Cod_vendedor    string `json:"cod_vendedor"`
    Cod_fornecedor  string `json:"cod_fornecedor"`
    Flag_rel        string `json:"flag"`
}

func (t* VendedorFornecedor) Save(ctx *iris.Context)(error){
    querySql := "INSERT INTO VENDEDOR_FORNECEDOR (COD_VENDEDOR,COD_FORNECEDOR,FLAG_REL) VALUES('" + t.Cod_vendedor + "','" + t.Cod_fornecedor
    querySql += "','" + t.Flag_rel+"');"    
    _, err := ctx.Get("db_vendas").(*sql.DB).Exec(querySql)
    //fmt.Printf("%s\n", querySql)

    if err != nil {
        return errors.New(err.Error())
    }
    return nil;
}

func (t * VendedorFornecedor) GetAll(ctx *iris.Context)([]VendedorFornecedor,error){
    result := []VendedorFornecedor{}    
    
    rows,err := ctx.Get("db_vendas").(*sql.DB).
            Query("SELECT * FROM VENDEDOR_FORNECEDOR");

    if err != nil { return result,errors.New(err.Error())}    
    
    var cod_vendedor,cod_fornecedor,flag string
    var id_vendedor_fornecedor int64
    
    for rows.Next(){			
        //fmt.Println("entrou")
        err := rows.Scan(&id_vendedor_fornecedor,&cod_vendedor,&cod_fornecedor,&flag)
        if err != nil {return result, errors.New(err.Error())}
        
        result = append(result, VendedorFornecedor{
            Id            : id_vendedor_fornecedor,
            Cod_vendedor  : cod_vendedor,
            Cod_fornecedor: cod_fornecedor,
            Flag_rel : flag,
        })
    }    
    
    return result, nil;
}

func (t * VendedorFornecedor) GetVendedoresIdFromFornecedor(ctx *iris.Context,id string)([]VendedorFornecedor,error){
    result := []VendedorFornecedor{}
    
    //usando o Query junto com o ? evita SQL INJECTION
    rows,err := ctx.Get("db_vendas").(*sql.DB).
            Query("SELECT COD_VENDEDOR, ID FROM VENDEDOR_FORNECEDOR WHERE COD_FORNECEDOR=?",id);

    if err != nil { return result,errors.New(err.Error())}    
    
    var cod_vendedor string
    var id_vendedor_fornecedor int64
    for rows.Next(){			
        //fmt.Println("entrou")
        err := rows.Scan(&cod_vendedor,&id_vendedor_fornecedor)
        if err != nil {return result, errors.New(err.Error())}
        
        result = append(result, VendedorFornecedor{
            Id            : id_vendedor_fornecedor,
            Cod_vendedor  : cod_vendedor,
        })
    }    
    
    return result, nil;
}

