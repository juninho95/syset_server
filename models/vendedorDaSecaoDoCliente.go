package models;

import(
    _ "fmt"
    "database/sql"
    "gopkg.in/kataras/iris.v6"
    "errors"
)

type VendedorDaSecaoDoCliente struct{
    Id          string `json:"id"`
    Id_FK       int64 `json:"chaveEstrangeira"`
    Cod_cliente string `json:"cod_cliente"`
    Cod_secao   string `json:"cod_secao"`    
}

func (t *VendedorDaSecaoDoCliente) Save(ctx *iris.Context)(error){
    _,err := ctx.Get("db_vendas").(*sql.DB).
        Exec("INSERT INTO CLI_FORN_VEND_SEC (ID_VEND_FORN,COD_CLIENTE,COD_SECAO) VALUES (?,?,?);",
        t.Id_FK,t.Cod_cliente,t.Cod_secao);
    if err != nil { return errors.New(err.Error())}

    return nil;
}