package models;

type VendedorDaSecDoCli struct{
        Pk              int64 `json:"pk"`
	IdCli   	string `json:"cliente"`
        NomeCli         string `json:"nomeCliente"`
	IdVend   	string `json:"vendedor"`
        NomeVend        string `json:"nomeVend"`
	IdFornecedor    string `json:"fornecedor"`
        NomeFornedor    string `json:"nomeFornecedor"`
	IdSecao   	string `json:"secao"`
        NomeSecao       string `json:"nomeSecao"`
}