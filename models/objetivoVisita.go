package models;

import(
    "fmt"
    "database/sql"
    "gopkg.in/kataras/iris.v6"
    "errors"
)

type ObjetivoVisita struct{
    Cod_Objetivo    int `json:"codObjetivo"`
    Des_objetivo    string `json:"desObjetivo"`
    
}

func (t * ObjetivoVisita) GetAll(ctx *iris.Context)([]ObjetivoVisita,error){
    fmt.Println("Estou aqui. Entrei")

    result := []ObjetivoVisita{}        
    rows,err := ctx.Get("db_vendas").(*sql.DB).
            Query("SELECT * FROM OBJETIVO_VISITA");
            
    fmt.Println("Executei a query")
    if err != nil { return result,errors.New(err.Error())}    
    
    var cod int
    var desc string

    for rows.Next(){			        
        err := rows.Scan(&cod,&desc)
        if err != nil {return result, errors.New("erro no banco:"+err.Error())}        
        result = append(result, ObjetivoVisita{
            Cod_Objetivo  : cod,
            Des_objetivo  : desc,
        })
    }    
    
    return result, nil;
}


