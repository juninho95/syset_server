package models;
import(
    "strconv"
    "database/sql"
    "gopkg.in/kataras/iris.v6"
    "errors"
);
type Cliente struct{
    Base
}

func (c *Cliente) GetById(ctx *iris.Context,id int)(error){
    rows, err := ctx.Get("db").(*sql.DB).Query("SELECT CLICOD, CLIDES FROM CLIENTE WHERE CLICOD=" + strconv.Itoa(id))
    
    if(err != nil){
	ctx.JSON(500, iris.Map{"error": err.Error()})
    }
    for rows.Next(){					
	err := rows.Scan(&c.Id, &c.Nome)
	
        if err != nil {
            return errors.New(err.Error());            
        }		
        break;
    }    
    return nil;    
}

func (c *Cliente) GetAll(ctx *iris.Context)([]Cliente, error){
    rows, err := ctx.Get("db").(*sql.DB).Query("SELECT CLICOD, CLIDES FROM CLIENTE ORDER BY CLIDES")

    var clis = []Cliente{}

    if(err != nil){
        return clis,errors.New(err.Error());        
    }

    cli := Cliente{}

    for rows.Next(){			
        err := rows.Scan(&cli.Id,&cli.Nome)
        clis = append(clis, cli)

        if err != nil {
            return clis,errors.New(err.Error())
        }
    }
    return clis,nil

}