package models;

import(
    "strconv"
    "database/sql"
    "gopkg.in/kataras/iris.v6"
    "errors"
);

type Secao struct{
    Base
}

func (s *Secao) GetById(ctx *iris.Context,id int)(error){
    rows, err := ctx.Get("db").(*sql.DB).Query("SELECT SECCOD, SECDES FROM SECAO WHERE SECCOD=" + strconv.Itoa(id))

    if(err != nil){
        return errors.New(err.Error())        
    }

    for rows.Next(){			
        err := rows.Scan(&s.Id, &s.Nome)
        if err != nil {            
            return errors.New(err.Error())        
        }
        break;
    }
    return nil;    
}

func (s *Secao) GetAll(ctx *iris.Context)([]Secao, error){
    rows, err := ctx.Get("db").(*sql.DB).Query("SELECT SECCOD, SECDES FROM SECAO ORDER BY SECDES")
    secs := []Secao{}
    
    if(err != nil){
        return secs,errors.New(err.Error())        
    }
    
    sec := Secao{}

    for rows.Next(){					
        err := rows.Scan(&sec.Id,&sec.Nome)
        secs = append(secs, sec)

        if err != nil {
            return secs,errors.New(err.Error())        
        }
    }
    return secs,nil
}