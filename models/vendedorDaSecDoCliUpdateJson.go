package models;

type VendedorDaSecDoCliUpdateJson struct{
    OldData VendedorDaSecDoCli `json:"oldData"`
    NewData VendedorDaSecDoCli `json:"newData"`
}
