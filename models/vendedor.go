package models;

import(
    "strconv"
    "database/sql"
    "gopkg.in/kataras/iris.v6"
    "errors"
);

type Vendedor struct {
	Base
}

func (v *Vendedor) GetById(ctx *iris.Context,id int)(error){
    rows, err := ctx.Get("db").(*sql.DB).Query("SELECT FUNCOD, FUNDES FROM FUNCIONARIO WHERE FUNCOD=" + strconv.Itoa(id))

    if(err != nil){
        return errors.New(err.Error())        
    }    
    for rows.Next(){			
        err := rows.Scan(&v.Id, &v.Nome)
        if err != nil {
            return errors.New(err.Error())        
        }		
        break;
    }
    return nil;    
}

func (v *Vendedor) GetAll(ctx *iris.Context)([]Vendedor, error){
    rows, err := ctx.Get("db").(*sql.DB).Query("SELECT FUNCOD, FUNDES FROM FUNCIONARIO ORDER BY FUNDES")
    vens := []Vendedor{}
    if(err != nil){
        return vens, errors.New(err.Error())
    }
    
    ven := Vendedor{}

    for rows.Next(){			
        err := rows.Scan(&ven.Id,&ven.Nome)
        vens = append(vens, ven)

        if err != nil {
            return vens, errors.New(err.Error())
        }
    }    
    return vens,nil
}