package models

import(
    "fmt"         
    "database/sql"   
   // "strconv"
    "gopkg.in/kataras/iris.v6"
    "errors"
);

type Relatorio struct{
    Cod_relatorio int64 `json:"cod_relatorio`
    Cod_vendedor  string `json:"cod_vendedor"`
    Data_inicio   string  `json:"dt_inicio"`
    Data_final    string `json:"dt_final"`
    Parecer       sql.NullString `json:"parecer"`
    Analista      sql.NullString `json:"analista"`
    Recomendacoes sql.NullString `json:"recomendacoes"`
    Observacoes   sql.NullString `json:"observacoes"`
    Status        string `json:"status"`
}


func (t *Relatorio) Save(ctx *iris.Context)(error){    
    _,err := ctx.Get("db_vendas").(*sql.DB).
        Exec("INSERT INTO RELATORIO (COD_VENDEDOR,DATA_INICIO,DATA_FINAL,STATUS) VALUES (?,?,?,?);",
            t.Cod_vendedor, t.Data_inicio,t.Data_final,t.Status);
            
    if err != nil { return errors.New(err.Error())}
    return nil;
}

func (t *Relatorio) UpdateStatus(ctx *iris.Context)(error){
    _,err := ctx.Get("db_vendas").(*sql.DB).Exec("UPDATE RELATORIO SET STATUS = ? WHERE COD_RELATORIO=?",t.Status,t.Cod_relatorio)
    return err
}

func (t *Relatorio) GetRelatorioPlanejadosByVendedor(ctx *iris.Context)([]Relatorio,error){
    res := []Relatorio{}
    rows,err := ctx.Get("db_vendas").(*sql.DB).Query("SELECT * FROM RELATORIO WHERE COD_VENDEDOR=? AND STATUS=?",t.Cod_vendedor,t.Status)
    
    if err != nil {return res, errors.New(err.Error())}
    aux := Relatorio{}
    for rows.Next(){			
        rows.Scan(&aux.Cod_relatorio,&aux.Cod_vendedor,&aux.Data_inicio,&aux.Data_final,&aux.Parecer,&aux.Analista,&aux.Recomendacoes,&aux.Observacoes,&aux.Status)
        res = append(res,aux);
    }
//    fmt.Println(res);
    return res,nil;
}

func (t *Relatorio) GetRelatorioByVendedor(ctx *iris.Context,id string)([]Relatorio,error){
    res := []Relatorio{}

    rows, err := ctx.Get("db_vendas").(*sql.DB).Query("SELECT * FROM RELATORIO WHERE COD_VENDEDOR=?",id)

    if(err != nil){return res,errors.New(err.Error())}    
    
    aux := Relatorio{}
    
 
 
    for rows.Next(){			
        rows.Scan(&aux.Cod_relatorio,&aux.Cod_vendedor,&aux.Data_inicio,&aux.Data_final,&aux.Parecer,&aux.Analista,&aux.Recomendacoes,&aux.Observacoes,&aux.Status)
        //Não precisa tratar o erro, pq no SCAN gera erro dos campos null
        //So ignorar os erros
        res = append(res,aux);
    }
    fmt.Println(res);
    return res,nil;
}