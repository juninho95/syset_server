package models;

type Usuario struct {
    Nome   string `json:"nome"`
    Senha  string `json:"senha"`
    Role   string `json:"role"`
}
