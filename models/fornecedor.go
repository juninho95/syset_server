package models;
import(
    "strconv"
    "database/sql"
    "gopkg.in/kataras/iris.v6"
    "errors"
)

type Fornecedor struct{
	Base
}

func (f *Fornecedor) GetById(ctx *iris.Context,id int)(error){
    rows, err := ctx.Get("db").(*sql.DB).Query("SELECT FORCOD, FORDES FROM FORNECEDOR WHERE FORCOD=" + strconv.Itoa(id))

    if(err != nil){        
        return errors.New(err.Error())        
    }
    for rows.Next(){			
        err := rows.Scan(&f.Id, &f.Nome)
        if err != nil {
            return errors.New(err.Error())                    
        }		
        break;
    }  
    return nil;    
}

func (f *Fornecedor) GetAll(ctx *iris.Context)([]Fornecedor, error){
    rows, err := ctx.Get("db").(*sql.DB).Query("SELECT FORCOD, FORDES FROM FORNECEDOR ORDER BY FORDES")
    forns := []Fornecedor{}

    if(err != nil){
        return forns, errors.New(err.Error())
    }
    
    forn := Fornecedor{}

    for rows.Next(){			
        err := rows.Scan(&forn.Id,&forn.Nome)
        forns = append(forns, forn)

        if err != nil {
            return forns, errors.New(err.Error());            
        }
    }
    //ctx.JSON(iris.StatusOK, forns)
    return forns,nil
}