package controllers

import (
    "fmt"        
    "gopkg.in/kataras/iris.v6"    
    "syset_server/models/relatorio"
    _ "os/exec"
    _ "os"
    _ "path/filepath"
    "math/rand"
)

type GeraPdfCtrl struct {
    Id string
}

func (t *GeraPdfCtrl) Register(app *iris.Framework){
    app.HandleFunc("POST","/api/sysET/gerarRelatorio",t.GerarPDF)    
    fmt.Println("Registrando controlador de geracao de pdf...");
}


const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandStringBytes(n int) string {
    b := make([]byte, n)
    for i := range b {
        b[i] = letterBytes[rand.Intn(len(letterBytes))]
    }
    return string(b)
}

func (t *GeraPdfCtrl) GerarPDF(ctx *iris.Context){
    json := models.VisitaRelatorioJSON{}
    err := ctx.ReadJSON(&json);
    if err != nil {
        fmt.Println("Erro:", err.Error());
    }
    for i, v := range json.Visita {
        fmt.Println("[%d] = ", i, v)
    }
    
    //fmt.Println("OLHA O JSON DA VISITA AQUI:",json.Visita);
    
    /*
    random := RandStringBytes(20);
    pdfFilename  := filepath.Join(os.TempDir(), random+".pdf")
    err := exec.Command("wkhtmltopdf","-O","Landscape","hello.html", pdfFilename).Run()

    if err == nil {
        fmt.Printf("hello.pdf succesfully generated")
        ctx.SendFile(pdfFilename,"relatorio.pdf")
    } else {
        fmt.Printf("Error on trying to generate PDF: %s", err)
    }
    */
}
