package controllers

import (
    "fmt"
    "syset_server/models"
    "gopkg.in/kataras/iris.v6"
)

type SecoesCtrl struct {
	Id string
}

func (t *SecoesCtrl) Register(app *iris.Framework){
	app.HandleFunc("GET","/api/sysPDV/secoes/{id}",t.byID)
	app.HandleFunc("GET","/api/sysPDV/secoes", t.All)
        fmt.Println("Registrando controlador de seção...");
}

func (t *SecoesCtrl) byID(ctx *iris.Context){	
    id, err1 :=  ctx.ParamInt("id")
    if err1 != nil{
        ctx.JSON(500, iris.Map{"error": "id deve ser um inteiro."})
    }
    sec := models.Secao{}
    err := sec.GetById(ctx,id);
    if(err != nil){
        ctx.JSON(500, iris.Map{"error": err.Error()})
        return
    }
    ctx.JSON(iris.StatusOK, sec)
}

func (t *SecoesCtrl) All(ctx *iris.Context){
    sec := models.Secao{}
    secs,err := sec.GetAll(ctx)
    if(err != nil){
        ctx.JSON(500, iris.Map{"error": err.Error()})
        return
    }

    ctx.JSON(iris.StatusOK, secs)
}


