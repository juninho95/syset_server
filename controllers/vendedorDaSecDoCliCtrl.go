package controllers

import (	
	"strings"
//	"fmt"
	"strconv"		
	"database/sql"
	"syset_server/models"
	"gopkg.in/kataras/iris.v6"
)

type VendedorDaSecDoCliCtrl struct{
    Id string
    Collections map[string]map[int]string
}

func (t *VendedorDaSecDoCliCtrl) Register(app *iris.Framework){
	app.HandleFunc("GET","/api/sysET/vendedorDaSecDoCliente", t.UpdateCollections, t.All) //, t.All) ISSO NAO FUNFA
	app.HandleFunc("POST","/api/sysET/vendedorDaSecDoCliente", t.Insert)
        app.HandleFunc("PUT", "/api/sysET/vendedorDaSecDoCliente", t.Update)
        app.HandleFunc("DELETE","/api/sysET/vendedorDaSecDoCliente", t.Delete)
}

func (t *VendedorDaSecDoCliCtrl) LoadCollection(ctx *iris.Context, collection, q string) error{
	var key,val string
	row2, err := ctx.Get("db").(*sql.DB).Query(q)

	if err != nil { return err}

	for row2.Next(){		
            erro := row2.Scan(&key, &val)
            if(erro != nil){ return erro}		
            key = strings.Replace(key," ","",-1)
            num ,err := strconv.Atoi(key)		
            if(err != nil){ return err}
            t.Collections[collection][num] = val
	}
	return nil
}

func (t *VendedorDaSecDoCliCtrl) UpdateCollections(ctx *iris.Context){	
	
    t.Collections = make(map[string]map[int]string)
    t.Collections["clientes"] = make(map[int]string)
    t.Collections["vendedores"] = make(map[int]string)
    t.Collections["fornecedores"] = make(map[int]string)
    t.Collections["secoes"] = make(map[int]string)

    if err := t.LoadCollection(ctx, "clientes", "SELECT CLICOD, CLIDES FROM CLIENTE") ; err != nil {
        ctx.JSON(500, iris.Map{"error": err.Error()})
        return;
    }
    if err := t.LoadCollection(ctx, "vendedores", "SELECT FUNCOD, FUNDES FROM FUNCIONARIO") ; err != nil {
        ctx.JSON(500, iris.Map{"error": err.Error()})
        return;
    }
    if err := t.LoadCollection(ctx, "fornecedores", "SELECT FORCOD, FORDES FROM FORNECEDOR") ; err != nil {
        ctx.JSON(500, iris.Map{"error": err.Error()})
        return;
    }
    if err := t.LoadCollection(ctx, "secoes", "SELECT SECCOD, SECDES FROM SECAO") ; err != nil {
        ctx.JSON(500, iris.Map{"error": err.Error()})
        return;
    }
    ctx.Next()
	
}

func (t *VendedorDaSecDoCliCtrl) trataChaveCollection(s string)(int,error){
	s = strings.Replace(s," ","",-1)
	num ,err := strconv.Atoi(s)		
	if err != nil {return 0, err}
	return num,nil	
}

func (t *VendedorDaSecDoCliCtrl) All(ctx *iris.Context){
	rows, err := ctx.Get("db_vendas").(*sql.DB).Query("SELECT * FROM VENDEDOR_DA_SECAO_DO_CLIENTE ORDER BY CLIENTE")

	if(err != nil){
            ctx.JSON(500, iris.Map{"error": err.Error()})
	}

	results := []models.VendedorDaSecDoCli{}
	var idVend, idCli, idFor, idSec string
        var pk int64
        var nomeVend, nomeCli, nomeFor, nomeSec string

	for rows.Next(){			
            //fmt.Println("entrou")
            err := rows.Scan(&pk,&idCli,&idFor,&idSec,&idVend)

            if err != nil {
                    ctx.JSON(500, iris.Map{"error": err.Error()})
                    return
            }
            //fmt.Println("idVend: ",idVend)

            vend,_ := t.trataChaveCollection(idVend)	
            nomeVend,_ = t.Collections["vendedores"][vend]

            cli,_ := t.trataChaveCollection(idCli)
            nomeCli,_ = t.Collections["clientes"][cli]

            forn,_ := t.trataChaveCollection(idFor)
            nomeFor,_ = t.Collections["fornecedores"][forn]

            sec,_ := t.trataChaveCollection(idSec)		
            nomeSec,_ = t.Collections["secoes"][sec]

            results = append(results, models.VendedorDaSecDoCli{
                    Pk               : pk,
                    IdCli            : idCli,
                    NomeCli          : nomeCli,                        
                    IdVend           : idVend,
                    NomeVend         : nomeVend,
                    IdFornecedor     : idFor,
                    NomeFornedor     : nomeFor,
                    IdSecao          : idSec,
                    NomeSecao        : nomeSec,
            })
	}

	ctx.JSON(iris.StatusOK, results)
}

func (t *VendedorDaSecDoCliCtrl) Insert(ctx *iris.Context){
    json := models.VendedorDaSecDoCli{}
    err := ctx.ReadJSON(&json)	

    if err != nil {
        ctx.JSON(500, iris.Map{"error": err.Error()})
    }

    querySql := "INSERT INTO VENDEDOR_DA_SECAO_DO_CLIENTE VALUES('" + json.IdCli + "','" + json.IdFornecedor
    querySql += "','" + json.IdSecao + "','" + json.IdVend +"');"

    _, err2 := ctx.Get("db_vendas").(*sql.DB).Exec(querySql)

    //fmt.Printf("%s\n", querySql)

    if err2 != nil {
        ctx.JSON(500, iris.Map{"error": err2.Error()})
    }else{
        ctx.JSON(200, iris.Map{"OK": "Inserido com sucesso"})
    }
}
func (t *VendedorDaSecDoCliCtrl) Update(ctx *iris.Context){
    json := models.VendedorDaSecDoCliUpdateJson{}
    err := ctx.ReadJSON(&json)
    if err != nil {ctx.JSON(500, iris.Map{"error": err.Error()});return}
    
    querySql := "UPDATE VENDEDOR_DA_SECAO_DO_CLIENTE ";
    querySql += "SET cliente='"+json.NewData.IdCli + "',";
    querySql += "fornecedor='"+json.NewData.IdFornecedor+ "',";
    querySql += "secao='"+json.NewData.IdSecao + "',";
    querySql += "vendedor='"+json.NewData.IdVend + "'";
    querySql += "WHERE cliente='"+json.OldData.IdCli+"' AND ";
    querySql += "fornecedor='"+json.OldData.IdFornecedor+ "' AND ";
    querySql += "secao='"+json.OldData.IdSecao + "' AND ";
    querySql += "vendedor='"+json.OldData.IdVend + "';";
    
    _, err2 := ctx.Get("db_vendas").(*sql.DB).Exec(querySql)

    if err2 != nil {
        ctx.JSON(500, iris.Map{"error": err2.Error()})
        return;
    }else{
        ctx.JSON(200, iris.Map{"OK": "Atualizado com sucesso"})
    }
}
func (t *VendedorDaSecDoCliCtrl) Delete(ctx *iris.Context){
    json := models.VendedorDaSecDoCli{}
    err := ctx.ReadJSON(&json)
    if(err != nil){ctx.JSON(500, iris.Map{"error": err.Error()});return}    

    querySql := "DELETE FROM VENDEDOR_DA_SECAO_DO_CLIENTE WHERE ";
    querySql += "cliente='"+json.IdCli+"' AND ";
    querySql += "vendedor='"+json.IdVend+"' AND ";
    querySql += "fornecedor='"+json.IdFornecedor+"' AND ";
    querySql += "secao='"+json.IdSecao+"';";

    _, err2 := ctx.Get("db_vendas").(*sql.DB).Exec(querySql)    
    if err2 != nil {
        ctx.JSON(500, iris.Map{"error": err2.Error()})
        return;
    }else{
        ctx.JSON(200, iris.Map{"OK": "Deletado com sucesso"})
    }
}