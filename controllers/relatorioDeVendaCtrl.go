package controllers

import (
    "fmt"
    "syset_server/models"
    "gopkg.in/kataras/iris.v6"
)

type RelatorioDeVendaCtrl struct {
	Id string
}

func (t *RelatorioDeVendaCtrl) Register(app *iris.Framework){	
    app.HandleFunc("GET" ,"/api/sysET/RelatorioDeVenda/objetivos", t.GetObjetivos)
    app.HandleFunc("GET" ,"/api/sysET/RelatorioDeVenda/visitaRealizada/{relatorio}",t.GetVisitaRealizadaFromRelatorio)
    app.HandleFunc("GET" ,"/api/sysET/RelatorioDeVenda/visitaRealizada/{relatorio}/{dia}",t.GetVisitaRealizadaDoDiaFromRelatorio)
    app.HandleFunc("GET" ,"/api/sysET/RelatorioDeVenda/visitaSemRealizacao/{cod}/{dia}",t.GetVisitaSemRealizacao)
    app.HandleFunc("GET" ,"/api/sysET/RelatorioDeVenda/visitaSemRealizacao/{cod}",t.GetVisitaSemRealizacaoFromRelatorio)
    app.HandleFunc("POST","/api/sysET/RelatorioDeVenda/visita", t.InsereVisita)
    app.HandleFunc("PUT" ,"/api/sysET/RelatorioDeVenda/visita",t.UpdateVisita)
    app.HandleFunc("GET" ,"/api/sysET/RelatorioDeVenda/visita/{relatorio}", t.GetAllVisitasFromRelatorio)
    app.HandleFunc("GET" ,"/api/sysET/RelatorioDeVenda/visita/{cod}/{dia}", t.GetPlanejamentoDoDia)
    app.HandleFunc("GET" ,"/api/sysET/RelatorioDeVenda/{id}",t.GetRelatorioDoVendedor)
    app.HandleFunc("POST","/api/sysET/RelatorioDeVenda", t.InserePlanejamento)
    app.HandleFunc("PUT" ,"/api/sysET/RelatorioDeVenda/status", t.UpdateStatus)        
    app.HandleFunc("GET","/api/sysET/RelatorioDeVenda/planejados/{cod}", t.GetRelatoriosPlanejadosDoVendedor)
    fmt.Println("Registrando controlador do Relatorio de Vendas...");
}



func (t *RelatorioDeVendaCtrl) UpdateVisita(ctx *iris.Context){
    json := models.Visita{}
    err := ctx.ReadJSON(&json)
    err = json.UpdateVisita(ctx)
    if(err != nil){ 
        ctx.JSON(500, iris.Map{"retornou erro": err.Error()})
        return;
    }
    ctx.JSON(iris.StatusOK, "Status Atualizado com sucesso!")
}

func(t *RelatorioDeVendaCtrl) GetAllVisitasFromRelatorio(ctx *iris.Context){
    model := models.Visita{}
    model.Cod_relatorio,_ = ctx.ParamInt64("relatorio")    
    res, err := model.GetAllVisitaFromRelatorio(ctx)
    if(err != nil){ 
        ctx.JSON(500, iris.Map{"Erro": err.Error()})
        return;
    }
    ctx.JSON(iris.StatusOK, res)
}

func (t *RelatorioDeVendaCtrl) GetVisitaRealizadaFromRelatorio(ctx *iris.Context){
    model := models.Visita{}
    model.Cod_relatorio,_ = ctx.ParamInt64("relatorio")    
    res, err := model.GetVisitaRealizadaFromRelatorio(ctx)
    if(err != nil){ 
        ctx.JSON(500, iris.Map{"Erro": err.Error()})
        return;
    }
    ctx.JSON(iris.StatusOK, res)
}

func (t *RelatorioDeVendaCtrl) GetVisitaRealizadaDoDiaFromRelatorio(ctx *iris.Context){
    model := models.Visita{}
    model.Cod_relatorio,_ = ctx.ParamInt64("relatorio")   
    model.Dia_da_semana,_ = ctx.ParamInt("dia")
    res, err := model.GetVisitaRealizadaDoDiaFromRelatorio(ctx)
    if(err != nil){ 
        ctx.JSON(500, iris.Map{"Erro": err.Error()})
        return;
    }
    ctx.JSON(iris.StatusOK, res)
}

func (t *RelatorioDeVendaCtrl) UpdateStatus(ctx *iris.Context){
    json := models.Relatorio{}
    err := ctx.ReadJSON(&json)
    err = json.UpdateStatus(ctx)
    if(err != nil){ 
        ctx.JSON(500, iris.Map{"retornou erro": err.Error()})
        return;
    }
    ctx.JSON(iris.StatusOK, "Status Atualizado com sucesso!")
}

func (t *RelatorioDeVendaCtrl) GetRelatoriosPlanejadosDoVendedor(ctx *iris.Context){
    model := models.Relatorio{}
    model.Cod_vendedor = ctx.Param("cod")
    model.Status = "Planejado"
    res, err := model.GetRelatorioPlanejadosByVendedor(ctx)
    if(err != nil){ 
        ctx.JSON(500, iris.Map{"Erro": err.Error()})
        return;
    }
    ctx.JSON(iris.StatusOK, res)
}

func (t *RelatorioDeVendaCtrl) GetPlanejamentoDoDia(ctx *iris.Context){
    obj := models.Visita{}
    obj.Dia_da_semana,_ = ctx.ParamInt("dia");
    obj.Cod_relatorio,_ = ctx.ParamInt64("cod");
    //fmt.Println("Recebi:",obj.Dia_da_semana,obj.Cod_relatorio)

    res,err := obj.GetVisitaPlanejadaByDiaAndCodRelatorio(ctx);
    if(err != nil){ 
        ctx.JSON(500, iris.Map{"retornou erro": err.Error()})
        return;
    }
    ctx.JSON(iris.StatusOK, res)    
}

func (t *RelatorioDeVendaCtrl) GetVisitaSemRealizacaoFromRelatorio(ctx *iris.Context){
    obj := models.Visita{}    
    obj.Cod_relatorio,_ = ctx.ParamInt64("cod");    

    res,err := obj.GetVisitaNaoRealizadaDoRelatorio(ctx);
    if(err != nil){ 
        ctx.JSON(500, iris.Map{"retornou erro": err.Error()})
        return;
    }
    ctx.JSON(iris.StatusOK, res) 
}

func (t *RelatorioDeVendaCtrl) GetVisitaSemRealizacao(ctx *iris.Context){
    obj := models.Visita{}
    obj.Dia_da_semana,_ = ctx.ParamInt("dia");
    obj.Cod_relatorio,_ = ctx.ParamInt64("cod");    

    res,err := obj.GetVisitaNaoRealizadaDoDiaByRelatorio(ctx);
    if(err != nil){ 
        ctx.JSON(500, iris.Map{"retornou erro": err.Error()})
        return;
    }
    ctx.JSON(iris.StatusOK, res)    
}

func (t *RelatorioDeVendaCtrl) GetObjetivos(ctx *iris.Context){
    //fmt.Println("estou no controller")
    obj := models.ObjetivoVisita{}

    objetivos,err := obj.GetAll(ctx)

    if(err != nil){ 
        ctx.JSON(500, iris.Map{"retornou erro": err.Error()})
        return;
    }
    ctx.JSON(iris.StatusOK, objetivos)
}


func (t * RelatorioDeVendaCtrl) GetRelatorioDoVendedor(ctx *iris.Context){
    id :=  ctx.Param("id");
    relatorioModel := models.Relatorio{}    
    res,err := relatorioModel.GetRelatorioByVendedor(ctx,id);    
    if err != nil {ctx.JSON(500, iris.Map{"error": "Erro interno"+err.Error()});return}    
    ctx.JSON(200,res);    
}

func (t *RelatorioDeVendaCtrl) InserePlanejamento(ctx *iris.Context){
    json := models.Relatorio{}
    err := ctx.ReadJSON(&json)	
    fmt.Println("olha o json: ",json);
    if err != nil {
        ctx.JSON(500, iris.Map{"error": "Erro no JSON:"+err.Error()})
    }    
    err = json.Save(ctx)
    if err != nil {
        ctx.JSON(500, iris.Map{"error": "Erro no Banco de dados:"+err.Error()})
    }else{
        ctx.JSON(200, iris.Map{"OK": "Inserido com sucesso"})
    }
}

func (t *RelatorioDeVendaCtrl) InsereVisita(ctx *iris.Context){
    json := models.Visita{}
    err := ctx.ReadJSON(&json)
    fmt.Println("olha o json da visita:",json);

    if err != nil {
        ctx.JSON(500, iris.Map{"error": "Erro no JSON:"+err.Error()})
        return;
    }    
    err = json.Save(ctx)
    if err != nil {
        ctx.JSON(500, iris.Map{"error": "Erro no Banco de dados:"+err.Error()})
    }else{
        ctx.JSON(200, iris.Map{"OK": "Inserido com sucesso"})
    }
}