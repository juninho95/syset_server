package controllers;

import (
    "fmt"
    "syset_server/models"
    "gopkg.in/kataras/iris.v6"
)

type VendedorFornecedorCtrl struct {
    Id string
}

func (t *VendedorFornecedorCtrl) Register(app *iris.Framework){
    //app.HandleFunc("GET","/api/sysPDV/fornecedores/{id}",t.byID)
    app.HandleFunc("GET","/api/sysET/vendedorFornecedor", t.All)
    app.HandleFunc("GET","/api/sysET/vendedorFornecedor/{id}", t.GetVendedoresIdFromFornecedor)
    app.HandleFunc("POST","/api/sysET/vendedorFornecedor", t.Insert)
    fmt.Println("Registrando controlador de Vendedor do Fornecedor...");

}

func (t * VendedorFornecedorCtrl) GetVendedoresIdFromFornecedor(ctx *iris.Context){

    id :=  ctx.Param("id")
    vendedorFornecedor := models.VendedorFornecedor{}    
    result,err := vendedorFornecedor.GetVendedoresIdFromFornecedor(ctx,id)
    if err != nil{ctx.JSON(500, iris.Map{"error": err.Error()})}    
    ctx.JSON(iris.StatusOK,result)
}


func (t * VendedorFornecedorCtrl) All(ctx *iris.Context){
    model := models.VendedorFornecedor{}
    result,err := model.GetAll(ctx);
    if err != nil{ctx.JSON(500, iris.Map{"error": err.Error()})}    
    ctx.JSON(iris.StatusOK,result)
}

func (t * VendedorFornecedorCtrl) Insert(ctx *iris.Context){
    json := models.VendedorFornecedor{}
    err := ctx.ReadJSON(&json)	
//    fmt.Println("olha o json: ",json);
    if err != nil {
        ctx.JSON(500, iris.Map{"error": err.Error()})
    }    
    err = json.Save(ctx)
    if err != nil {
        ctx.JSON(500, iris.Map{"error": err.Error()})
    }else{
        ctx.JSON(200, iris.Map{"OK": "Inserido com sucesso"})
    }
}