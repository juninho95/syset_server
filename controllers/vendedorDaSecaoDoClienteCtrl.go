package controllers;
import (
    "fmt"
    "syset_server/models"
    "gopkg.in/kataras/iris.v6"
)

type VendedorDaSecaoDoClienteCtrl struct {
    Id string
}

func (t *VendedorDaSecaoDoClienteCtrl) Register(app *iris.Framework){  
    app.HandleFunc("POST","/api/sysET/vendedorDaSecaoDoCliente", t.Insert)    
    fmt.Println("Registrando controlador de Vendedor da secao do cliente...");
}

func (t *VendedorDaSecaoDoClienteCtrl) Insert(ctx *iris.Context){
    json := models.VendedorDaSecaoDoCliente{}
    err := ctx.ReadJSON(&json)	
    fmt.Println("olha o json: ",json);
    if err != nil {ctx.JSON(500, iris.Map{"error": err.Error()})}    
    err = json.Save(ctx)

    if err != nil {
        ctx.JSON(500, iris.Map{"error": err.Error()})
    }else{
        ctx.JSON(200, iris.Map{"OK": "Inserido com sucesso"})
    }
}