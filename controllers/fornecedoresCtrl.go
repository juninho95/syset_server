package controllers

import (
    "fmt"
    "syset_server/models"
    "gopkg.in/kataras/iris.v6"
)

type FornecedoresCtrl struct {
	Id string
}

func (t *FornecedoresCtrl) Register(app *iris.Framework){
	app.HandleFunc("GET","/api/sysPDV/fornecedores/{id}",t.byID)
	app.HandleFunc("GET","/api/sysPDV/fornecedores", t.All)
        fmt.Println("Registrando controlador de fornecedores...");
}

func (t *FornecedoresCtrl) byID(ctx *iris.Context){	

    id, err1 :=  ctx.ParamInt("id")
    if err1 != nil{
        ctx.JSON(500, iris.Map{"error": "id deve ser um inteiro."})
    }
    forn := models.Fornecedor{}
    err := forn.GetById(ctx,id);
    if err != nil {ctx.JSON(500, iris.Map{"error": err.Error()});return}
    ctx.JSON(iris.StatusOK, forn)
}

func (t *FornecedoresCtrl) All(ctx *iris.Context){
    forn := models.Fornecedor{}
    forns,err := forn.GetAll(ctx);
    if(err != nil){
        ctx.JSON(500, iris.Map{"error": err.Error()});
        return;
    }
    ctx.JSON(iris.StatusOK, forns)
}
