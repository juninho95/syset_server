package controllers

import (
    "fmt"
    "syset_server/models"
    "gopkg.in/kataras/iris.v6"
)

type ClientesCtrl struct {
    Id string
}

func (t *ClientesCtrl) Register(app *iris.Framework){
    app.HandleFunc("GET","/api/sysPDV/clientes/{id}",t.byID)
    app.HandleFunc("GET","/api/sysPDV/clientes", t.All)
    fmt.Println("Registrando controlador de clientes...");
}

func (t *ClientesCtrl) byID(ctx *iris.Context){	

    var cli = models.Cliente{}
    id, err1 :=  ctx.ParamInt("id")
    if err1 != nil{
        ctx.JSON(500, iris.Map{"error": err1.Error()});     
    }
    err1 = cli.GetById(ctx,id);	
    if err1 != nil {ctx.JSON(500, iris.Map{"error": err1.Error()})}
    ctx.JSON(iris.StatusOK, cli)
}

func (t *ClientesCtrl) All(ctx *iris.Context){        
    var cli = models.Cliente{}
    clis, err := cli.GetAll(ctx)
    if(err != nil){
        ctx.JSON(500, iris.Map{"error": err.Error()})
    }	
    ctx.JSON(iris.StatusOK, clis)
}
