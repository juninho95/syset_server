package controllers

import (	
    "fmt"
    "syset_server/models"
    "gopkg.in/kataras/iris.v6"
)

type VendedoresCtrl struct {
	Id string
}

func (t *VendedoresCtrl) Register(app *iris.Framework){
	app.HandleFunc("GET","/api/sysPDV/vendedores/{id}", t.byID)
	app.HandleFunc("GET","/api/sysPDV/vendedores", t.All)
        fmt.Println("Registrando controlador de funcionarios...");
}

func (t *VendedoresCtrl) byID(ctx *iris.Context){	
    id, err1 :=  ctx.ParamInt("id")
    if err1 != nil{
        ctx.JSON(500, iris.Map{"error": "id deve ser um inteiro."})
    }
    
    ven := models.Vendedor{}	
    err := ven.GetById(ctx,id)
    if err!= nil{
        ctx.JSON(500, iris.Map{"error": err.Error()})
        return
    }
    ctx.JSON(iris.StatusOK, ven)
}

func (t *VendedoresCtrl) All(ctx *iris.Context){
	
	
	ven := models.Vendedor{}
        vens,err := ven.GetAll(ctx)
        if(err != nil){ 
            ctx.JSON(500, iris.Map{"error": err.Error()})
            return;
        }

	ctx.JSON(iris.StatusOK, vens)
}
