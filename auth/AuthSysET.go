package auth;

import(
    _ "fmt"
    "gopkg.in/kataras/iris.v6"		
    "gopkg.in/kataras/iris.v6/adaptors/sessions"
    "github.com/gorilla/securecookie"
    "syset_server/models"
    "database/sql"
    "strings"
)


type AuthSysET struct{
    cookieName string
    hashKey []byte
    blockKey []byte
}

func (t *AuthSysET) Login(ctx *iris.Context) error{
    
    var senha,nome,role string
    //fmt.Println("estou no login")
    
    json := models.Usuario{}    
    err := ctx.ReadJSON(&json)	

    //fmt.Println("nome:",json.Nome,"senha:",json.Senha)

    if err != nil {
        ctx.JSON(500, iris.Map{"error": err.Error()})
    }
    
    //fmt.Println("SELECT COD_USUARIO, SENHA FROM USUARIO WHERE COD_USUARIO="+json.Nome)
    row, err := ctx.Get("db_vendas").(*sql.DB).Query("SELECT COD_USUARIO, SENHA, ROLE FROM USUARIO WHERE COD_USUARIO=?",json.Nome)
    if (err != nil) { return err }
    
    if row.Next(){ //Se achou um registro
        //fmt.Println("AChou Registro")
        erro := row.Scan(&nome, &senha, &role)
        //fmt.Println("nome:",nome)
        //fmt.Println("senha:","a",senha,"a")
        //fmt.Println("senha:","a",,"a")
        //fmt.Println("recebida:","a",json.Senha,"a")
        if(erro != nil){ return erro }
        if(strings.Trim(senha," ") == json.Senha){
//            fmt.Println("Logado, criando sessao");
            ctx.Session().Set("logado", true)   // Senha bateu
            ctx.JSON(iris.StatusOK, models.Usuario{
                Nome : nome,
                Role : role,
            });            
        }else{
//            fmt.Println("Nao AChou Registro")
            ctx.JSON(401, iris.Map{"error": "Usuario ou senha incorreta"})
        }
    }else{        
        //Manda erro de senha ou usuário incorreto
        ctx.JSON(401, iris.Map{"error": "Usuario ou senha incorreta"})
    }
    
    return nil
}

func (t *AuthSysET) Serve(ctx *iris.Context){
    logged, _ := ctx.Session().GetBoolean("logado")	 //se não encontrar essa chave, retorna false por default
    
    if logged { 
        //fmt.Println("passou pelo AUTH")
        ctx.Next()
        return
    }else{	
        //Se não estiver logado, vai para a página de Login
        //fmt.Println("não passou pelo AUTH")
        ctx.JSON(401, iris.Map{"error": "Usuario não autenticado"})
        //ctx.ServeFile("static/index.html", false)
    }
}

func (t *AuthSysET) Register(app *iris.Framework){

    t.cookieName = "mycustomsessionid"
    // AES only supports key sizes of 16, 24 or 32 bytes.
    // You either need to provide exactly that amount or you derive the key from what you type in.
    t.hashKey = []byte("the-big-and-secret-fash-key-here")
    t.blockKey = []byte("lot-secret-of-characters-big-too")
    secureCookie := securecookie.New(t.hashKey, t.blockKey)

    mySessions := sessions.New(sessions.Config{
        Cookie: t.cookieName,
        Encode: secureCookie.Encode,
        Decode: secureCookie.Decode,
    })

    app.Adapt(mySessions)
}
