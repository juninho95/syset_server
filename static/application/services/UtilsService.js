/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var app = angular.module("App");

app.service("UtilsService",function(){
    
    this.contabilizaClientes = function(visitas, vend_forn) {        
        if(visitas.length === 0){
            return undefined;
        }
        var clientes = {}
        , fornecedores = {}
        , item;
        
        function push(list, element) {
            if (list[element.idFor] === undefined) {
                list[element.idFor.trim()] = element;
            }
        }
        var fornecedor, b1;

        while (item = visitas.shift()) {
            var cliente = vend_forn.find(function (element) {
                if (element.pk === item.pkCliFornVendSec) {
                    return true;
                }
            })
            //console.log("olha o dado", item);            
            if (clientes[cliente.cliente + item.diaDaSemana.toString()] === undefined) {
                clientes[cliente.cliente + item.diaDaSemana.toString()] = {nomeCliente: cliente.nomeCliente, objetivo: item.ObjVisita, codRelatorio: item.codRelatorio ,fornecedores: {}};
            }            
        }
        return clientes;
    }         
    
    /**
     * 
     * @param {type} visitas
     * @param {type} vend_forn
     * @returns retorna (as strings e não os IDs) dos clientes, seus fornecedor e valor de venda pretendido
     */
    this.joinVisitaVendFornecedor = function(visitas, vend_forn) {        
        if(visitas.length === 0){
            return undefined;
        }
        
        var clientes = {}
        , fornecedores = {}
        , item;
        
        function push(list, element) {
            if (list[element.idFor] === undefined) {
                list[element.idFor.trim()] = element;
            }
        }
        var fornecedor, b1;

        while (item = visitas.shift()) {
            var cliente = vend_forn.find(function (element) {
                if (element.pk === item.pkCliFornVendSec) {
                    return true;
                }
            })
            //console.log("olha o dado", item);
            if (clientes[cliente.cliente] === undefined) {
                clientes[cliente.cliente] = {nomeCliente: cliente.nomeCliente, objetivo: item.ObjVisita,diaDaSemana:item.diaDaSemana ,codRelatorio: item.codRelatorio ,fornecedores: {}};
            }
            b1 = cliente.cliente + cliente.fornecedor;
            if (fornecedores[b1] === undefined) {
                fornecedores[b1] = {idFor: cliente.fornecedor, codVisita: item.cod, pk: cliente.pk, nomeFor: cliente.nomeFornecedor, vlrVenda: item.vlrVenda};
            }
            fornecedor = fornecedores[b1];
            push(clientes[cliente.cliente].fornecedores, fornecedor);
        }
        return clientes;
    }            
    
    this.getObjetivoToString = function(objetivos,cod){
        //console.log("obj",objetivos)
        var res = objetivos.find(function(element){
            return element.codObjetivo === cod;
        })
        return res.desObjetivo;
    }
    
    
    
    
    this.getAllVisitasRealizada = function(visitas,vend_forn,cod_relatorio){
        var visitasFiltrada = visitas.filter(function(element){
            return element.tipo.trim() === 'R' || element.tipo.trim() === 'N';
        });
        
        var visitasFiltradaPorDia = [];
        var resultado = {}
        for(var i = 1; i < 6;i++){
            visitasFiltradaPorDia = visitasFiltrada.filter(function(element){
                return element.diaDaSemana === i;
            })            
            resultado[cod_relatorio.toString()+i.toString()] = this.joinVisitaVendFornecedor(visitasFiltradaPorDia,vend_forn)
        }
        return resultado;
    }
    
    
    
    this.getAllVisitasPlanejadas = function(visitas,vend_forn,cod_relatorio){
        var visitasFiltrada = visitas.filter(function(element){
            return element.tipo.trim() === 'P';
        });
        
        var visitasFiltradaPorDia = [];
        var resultado = {}
        for(var i = 1; i < 6;i++){
            visitasFiltradaPorDia = visitasFiltrada.filter(function(element){
                return element.diaDaSemana === i;
            })            
            resultado[cod_relatorio.toString()+i.toString()] = this.joinVisitaVendFornecedor(visitasFiltradaPorDia,vend_forn)
        }
        return resultado;
    }
})

