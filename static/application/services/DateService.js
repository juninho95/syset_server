/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var app = angular.module("App");

app.service('DateService', function () {
    this.dateToTimeStamp  = function(data){
        return [("0" + data.getDate()).slice(-2), ("0" + (data.getMonth() + 1)).slice(-2), data.getFullYear() % 100].join(".") + " "+data.getHours()+":"+data.getMinutes()
    }
});

