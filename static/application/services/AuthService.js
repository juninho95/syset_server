/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var app = angular.module("App");

app.service('AuthService', function ($http) {
    this.login = function (user) {
        console.log("mandando user:", user);
        return $http({
            method: 'POST',
            url: "/api/authentication",
            data: user
        });
    };
    this.logout = function () {
        //Deleta sessão no servidor
        return $http({
            method: 'DELETE',
            url: "/api/authentication/destroy"
        });
    }
})
