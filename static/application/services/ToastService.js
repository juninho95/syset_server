/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var app = angular.module("App");

app.service('ToastService', function (ngToast) {
    this.onSuccess = function(msg){
        ngToast.create({
            className: 'success',
            content: '<strong>Sucesso!</strong> ' + msg
        });        
    }
    
    this.onError = function(error){
        ngToast.create({
            className: 'danger',
            content: '<strong>Erro!</strong> ' + error
        });
    }

})