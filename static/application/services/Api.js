var app = angular.module("App");

app.service('ApiService', function ($http) {

    var request = function (url) {
        return $http({
            method: 'GET',
            url: url
        });
    }
    
    var postRequest = function (url, data) {
        return $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': 'application/json'},
            data: data
        })
    }
    
    var putRequest = function(url, data){
        return $http({
            method: 'PUT',
            url: url,
            headers: {'Content-Type': 'application/json'},
            data: data
        })
    }
    
    var deleteRequest = function(url,data){
        return $http({
            method: 'DELETE',
            url: url,
            headers: {'Content-Type': 'application/json'},
            data: data
        })
    }

    this.secoes = function (id) {
        if (typeof id != 'undefined') {
            return request('/api/sysPDV/secoes/' + id)
        } else {
            return request('/api/sysPDV/secoes')
        }
    }

    this.fornecedores = function (id) {
        if(id) {
            return request('/api/sysPDV/fornecedores/' + id)
        } else {
            //console.log("eh indefinido")
            return request('/api/sysPDV/fornecedores')
        }
    }

    this.clientes = function (id) {
        if (id) {
            return request('/api/sysPDV/clientes/' + id)
        } else {
            return request('/api/sysPDV/clientes')
        }
    }

    this.vendedores = function (id) {
        //console.log("ID:",id)
        if (id) {
          //  console.log("mandando:",'/api/sysPDV/vendedores/' + id)
            return request('/api/sysPDV/vendedores/' + id)
        } else {
            //console.log("TUDO")
            return request('/api/sysPDV/vendedores')
        }
    }

    this.insereVendedorDaSecaoDoCliente = function (data) {
        //console.log("inserindo ",data)
        return postRequest('/api/sysET/vendedorDaSecaoDoCliente', data)
    }
    
    this.insereVendedorFornecedor = function(data){
        return postRequest('/api/sysET/vendedorFornecedor',data);
    }
    
    this.vendedorDaSecDoCliente = function () {
        return request('/api/sysET/vendedorDaSecDoCliente');
    }
    
    this.vendedoresDoFornecedor = function(forn){
        return request('/api/sysET/vendedorFornecedor/'+forn);
    }
    
    this.inserePlanejamentoDoRelatorio = function(data){
        return postRequest('/api/sysET/RelatorioDeVenda',data)        
    }
    /**
     * 
     * @param {type} idVendedor
     * @returns Retorna todos os relatórios de um vendedor
     */
    this.getPlanejamentoDoVendedor = function(idVendedor){
        return request('/api/sysET/RelatorioDeVenda/'+idVendedor)
    }
    
    /**
     * 
     * 
     * Insere Uma Visita na fase de planejamento
     */
    
    this.insereVisitaPlanejadaNoRelatorio = function(data){
        return postRequest('/api/sysET/RelatorioDeVenda/visita',data)
    }
    
    /**
     * 
     * @returns Retorna os objetivos possíveis de uma visita (Cadastrado na tabela OBJETIVO_VISITA)
     */
    this.getObjetivoVisita = function(){
        return request('/api/sysET/RelatorioDeVenda/objetivos');
    }
    
    /**
     * 
     * @param {type} codRelatorio
     * @param {type} codDiaDaSemana
     * @returns Retorna as vendas planejadas de um dia específico de um relatório
     */
    this.getPlanejamentoDoDia = function(codRelatorio,codDiaDaSemana){
        return request('/api/sysET/RelatorioDeVenda/visita/'+codRelatorio + '/' + codDiaDaSemana)
    }
    
    /**
     * 
     * @param {type} codRelatorio
     * @param {type} codDiaDaSemana
     * @returns Retorna Todas as visitas planejadas e que não teve prestacao de contas em rotas realizadas de
     * um dia específica e de uma relatório específico
     */
    this.getVisitasSemRealizacao = function(codRelatorio,codDiaDaSemana){
        return request('/api/sysET/RelatorioDeVenda/visitaSemRealizacao/'+codRelatorio+'/'+codDiaDaSemana)
    }
    
    /**
     * 
     * @param {type} codRelatorio
     * @returns Retorna todas as visitas planejas que não teve prestação de contas em rotas realizadas de um 
     * relatório
     */
    this.getAllVisitasSemRealizacao = function(codRelatorio){
        return request('/api/sysET/RelatorioDeVenda/visitaSemRealizacao/'+codRelatorio)
    }
    
    /**
     * 
     * @param {type} codRelatorio
     * @returns Retorna todas as visitas de um relatório (REALIZADA, PLANEJADA e NAO-REALIZADA)
     */
    this.getAllVisitaFromRelatorio = function(codRelatorio){
        return request('/api/sysET/RelatorioDeVenda/visita/'+codRelatorio)
    }
    
    /**
     * 
     * @param {type} json
     * @returns Atualiza o status do relatório que é enviado no JSON.
     */
    this.atualizaStatusRelatorio = function(json){
        return putRequest('/api/sysET/RelatorioDeVenda/status',json)
    }
    
    /**
     * 
     * @param {type} cod_vendedor
     * @returns Retorna todos os relatório que estão com o Status Planejado
     */
    this.getRelatorioPlanejadoDoVendedor = function(cod_vendedor){
        return request('/api/sysET/RelatorioDeVenda/planejados/'+cod_vendedor)
    }    
    
    /**
     * 
     * @param {type} codRelatorio
     * @returns Retorna todas as visitas realizadas
     */
    this.getVisitaRealizadaFromRelatorio = function(codRelatorio){
        return request('/api/sysET/RelatorioDeVenda/visitaRealizada/'+codRelatorio)
    }
    
    /**
     * 
     * @returns Retorna as rotas realizadas de um dia de um relatório especifíco
     */
    this.getVisitaRealizadaDoDiaFromRelatorio = function(codRelatorio,diaDaSemana){
        return request('/api/sysET/RelatorioDeVenda/visitaRealizada/'+codRelatorio+'/'+diaDaSemana)
    }
    
    /**
     * 
     * Atualiza uma Visita. O cod de visita é passado no JSON e o novo valor é os valores passado no JSON     
     */
    this.updateVisita = function(json){
        return putRequest('/api/sysET/RelatorioDeVenda/visita',json)
    }
    
    //Manda dados para o servidor gerar o relatorio
    this.gerarRelatorio = function(json){
        return postRequest('/api/sysET/gerarRelatorio',json)
    }
});