/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var app = angular.module("App");
app.controller("RotaRealizadaCtrl", function ($scope, $q, UtilsService, ApiService, $rootScope, ToastService, $timeout, DateService) {

    var user = $rootScope.usuario;
    $scope.tabs = [
        {title: 'Segunda'},
        {title: 'Terça'},
        {title: 'Quarta'},
        {title: 'Quinta'},
        {title: 'Sexta'}
    ];
    $scope.diaDaSemana = 1;
    $scope.headerTable;
    $scope.model = {
        name: 'Tabs'
    };
    //Armazena os dados da tabela de rota realizada do vendedor
    $scope.tableData = {}
    
    $scope.tableDataRealizado = {}
    
    //Armazena os dados da tabela do planejamento de rotas do vendedor
    $scope.tableDataPlanejamento = {}

    var vendedorClienteFornecedor = [];
    $scope.relatorioCurrent;
    $scope.clienteFornecedorDoVendedor;
    $scope.fornecedorDoVendedorFiltrado;
    $scope.objetivos;
    var fornecedoresDoVendedor;
    $scope.novo = {
        cliente: {}
    }

    $scope.calculaVisitasRealizadas = function (relatorio) {
        ApiService.getAllVisitaFromRelatorio(relatorio.Cod_relatorio)
                .then(function successCallback(response) {
                    var arrayDePlanejados = response.data.filter(function (element) {
                        if (element.tipo.trim() === 'P') {
                            return true;
                        }
                    })
                    var resPlanejados = UtilsService.contabilizaClientes(arrayDePlanejados, vendedorClienteFornecedor)
                    if(resPlanejados){
                        relatorio.totalVisitas = Object.keys(resPlanejados).length;
                    }else{
                        relatorio.totalVisitas = 0;
                    }

                    var arrayDeRealizados = response.data.filter(function (element) {
                        if (element.tipo.trim() === 'R' || element.tipo.trim() === 'N') { //R - Realizado N- Nao realizado
                            return true;
                        }
                    })

                    var resRealizados = UtilsService.contabilizaClientes(arrayDeRealizados, vendedorClienteFornecedor)

                    if (resRealizados) {
                        relatorio.visitaRealizadas = Object.keys(resRealizados).length;
                    } else {
                        relatorio.visitaRealizadas = 0;
                    }

                }, function errorCallback(response) {
                    console.log("error: ", response)
                });
    }

    $scope.cadastrarVisitaNaoRealizada = function (json, relatorioPlan, diaDaSemana) {
        //console.log('json do cadastro', json);
        for (var x in json.cliente.fornecedores) {
            var temp = {
                codVisitaOcorrenciaPlan: {valid: true, int64: json.cliente.fornecedores[x].codVisita},
                diaDaSemana: diaDaSemana,
                ObjVisita: json.objetivo.codObjetivo,
                codRelatorio: relatorioPlan.Cod_relatorio,
                pkCliFornVendSec: json.cliente.fornecedores[x].pk,
                descVisitaRealizada: {valid: true, string: "Visita não realizada"},
                vlrVenda: 0,
                tipo: 'N',
                dataRegistro: DateService.dateToTimeStamp(new Date())
            };

            //Se já tiver sido cadastrado essa visita, chama o atualiza
            if (json.cliente.visitado) {
                temp.cod = json.cliente.fornecedores[x].cod;
                ApiService.updateVisita(temp)
                        .then(function successCallback(response) {
                            ToastService.onSuccess("Visita alterada Com sucesso!")
                        }, function errorCallback(response) {
                            ToastService.onError("Erro ao alterar visita!")
                            console.log(response)
                        });

            } else { //Caso contrário chama o inserir Nova
                ApiService.insereVisitaPlanejadaNoRelatorio(temp)
                        .then(function successCallback(response) {
                            ToastService.onSuccess("Visita Cadastrada Com sucesso!")
                            json.cliente.visitado = true;
                        }, function errorCallback(response) {
                            ToastService.onError("Erro ao cadastrar visita!")
                            console.log(response)
                        });
            }
        }
        relatorioPlan.visitaRealizadas++;
        //delete json;
    }

    $scope.cadastrarVisitaRealizada = function (json, relatorioPlan, diaDaSemana) {
        console.log('json do cadastro', json);

        for (var x in json.cliente.fornecedores) {
            var temp = {
                codVisitaOcorrenciaPlan: {valid: true, int64: json.cliente.fornecedores[x].codVisita},
                diaDaSemana: diaDaSemana,
                ObjVisita: json.objetivo.codObjetivo,
                codRelatorio: relatorioPlan.Cod_relatorio,
                pkCliFornVendSec: json.cliente.fornecedores[x].pk,
                descVisitaRealizada: {valid: true, string: json.cliente.desc},
                vlrVenda: parseFloat(json.cliente.fornecedores[x].vlrVendaRealizado.replace(/,/g, '.')),
                tipo: 'R',
                dataRegistro: DateService.dateToTimeStamp(new Date())
            };

            //Se já tiver sido cadastrado essa visita, chama o atualiza
            if (json.cliente.visitado) {
                temp.cod = json.cliente.fornecedores[x].cod;
                ApiService.updateVisita(temp)
                        .then(function successCallback(response) {
                            ToastService.onSuccess("Visita alterada Com sucesso!")
                        }, function errorCallback(response) {
                            ToastService.onError("Erro ao alterar visita!")
                            console.log(response)
                        });

            } else { //Caso contrário chama o inserir Nova
                ApiService.insereVisitaPlanejadaNoRelatorio(temp)
                        .then(function successCallback(response) {
                            ToastService.onSuccess("Visita Cadastrada Com sucesso!")
                            json.cliente.visitado = true;
                        }, function errorCallback(response) {
                            ToastService.onError("Erro ao cadastrar visita!")
                            console.log(response)
                        });
            }
        }
        relatorioPlan.visitaRealizadas++;
        //delete json;
    }

    $scope.setCliente = function (cliente) {

        console.log("SETANDO CLIENTE:", cliente)

        $scope.novo.cliente = cliente;
        $scope.novo.objetivo = $scope.objetivos.find(function (element) {
            return element.codObjetivo === cliente.objetivo;
        });

        //Caso já tenha sido cadastrado, preeche para o cara poder alterar
        if (cliente.visitado) {
            $scope.novo.botao = "Salvar"
            $scope.novo.header = "Alterar visita realizada"
            //busca valores do realizado
            ApiService.getVisitaRealizadaFromRelatorio(cliente.codRelatorio)
                    .then(function successCallback(response) {
                        var visitasRealizadas = response.data;
                        console.log('visitas realizadas:', visitasRealizadas)
                        for (var x in cliente.fornecedores) {
                            for (var y in visitasRealizadas) {
                                if (visitasRealizadas[y].pkCliFornVendSec === cliente.fornecedores[x].pk &&
                                        visitasRealizadas[y].diaDaSemana === cliente.diaDaSemana) {
                                    cliente.fornecedores[x].vlrVendaRealizado = visitasRealizadas[y].vlrVenda;
                                    cliente.fornecedores[x].cod = visitasRealizadas[y].cod;
                                    cliente.desc = visitasRealizadas[y].descVisitaRealizada.String;
                                    break;
                                }
                            }
                        }
                    }, function errorCallback(response) {
                        console.log("error: ", response)
                    });
        } else {
            $scope.novo.botao = "Gravar"
            $scope.novo.header = "Cadastrar visita realizada"
        }

    }

    ApiService.getObjetivoVisita()
            .then(function successCallback(response) {
                $scope.objetivos = response.data;
                //   console.log(response.data)
            }, function errorCallback(response) {
                console.log("error: ", response)
            });

    ApiService.vendedorDaSecDoCliente()
            .then(function successCallback(response) {
                vendedorClienteFornecedor = response.data;
                var aux = []

                $scope.clienteFornecedorDoVendedor = response.data.filter(function (obj) {
                    if (!aux.includes(obj.cliente) && user.codUsuario.trim() === obj.vendedor.trim()) {
                        aux.push(obj.cliente);
                        return obj;
                    }
                });
                aux = []
                //Pega todos os fornecedores do Vendedor, dps passa por uma filtragem
                fornecedoresDoVendedor = $scope.clienteFornecedorDoVendedor.filter(function (obj) {
                    if (!aux.includes(obj.fornecedor)) { //Equivalente ao contains do JAVA
                        aux.push(obj.fornecedor);
                        return obj;
                    }
                });
                //Seta o cabeçalho da tabela das metas do vendedor
                $scope.headerTable = [{cabecalho: 'Cliente'}];
                for (var x in fornecedoresDoVendedor) {
                    $scope.headerTable.push(fornecedoresDoVendedor[x]);
                }
            }, function errorCallback(response) {
                console.log("error: ", response)
            });

    ApiService.getPlanejamentoDoVendedor($rootScope.usuario.codUsuario)
            .then(function successCallback(response) {
                //console.log("passando:",$rootScope.usuario.codUsuario)
                //console.log("resposta do server:",response.data)
                $scope.planejamentos = response.data.filter(function (element) {
                    return element.status.trim() === 'Enviado para análise' || element.status.trim() === 'Planejado'
                });
            }, function errorCallback(response) {
                console.log(response)
            })

    $scope.buscaPlanejamentoRealizado = function (codRelatorio, codDiaDaSemana) {
        console.log("chamando para o dia ", codDiaDaSemana)
        if ($scope.tableDataRealizado[codRelatorio + codDiaDaSemana]) {
            return;//Se já processo os dados da tabela desse dia da semana, retorne
        } else {
            ApiService.getVisitaRealizadaDoDiaFromRelatorio(codRelatorio, codDiaDaSemana)
                    .then(function successCallback(response) {
                        if (response.data.length !== 0)
                            $scope.tableDataRealizado[codRelatorio + codDiaDaSemana] = UtilsService.joinVisitaVendFornecedor(response.data, vendedorClienteFornecedor);
                    }, function errorCallback(response) {
                        console.log(response)
                    })
        }
    }

    $scope.buscaPlanejamento = function (codRelatorio, codDiaDaSemana) {
        var d = $q.defer()
        ApiService.getPlanejamentoDoDia(codRelatorio, codDiaDaSemana)
                .then(function successCallback(response) {
                    $scope.tableDataPlanejamento[codRelatorio.toString() + codDiaDaSemana.toString()] = UtilsService.joinVisitaVendFornecedor(angular.copy(response.data), vendedorClienteFornecedor);
                    d.resolve(response);
                }, function errorCallback(response) {
                    console.log(response)
                    d.reject(response);
                });
        return d.promise;
    }

    $scope.buscaVisitaRealizada = function (codRelatorio, codDiaDaSemana, plan) {

        $scope.buscaPlanejamento(codRelatorio, codDiaDaSemana).then(function () {

            $scope.tableData = angular.copy($scope.tableDataPlanejamento);
            ApiService.getVisitasSemRealizacao(codRelatorio, codDiaDaSemana)
                    .then(function successCallback(response) {
                        //Caso todas as visitas desse dia tenha sido cadastradas, marque todos os clientes como visitado
                        if (response.data.length === 0) {
                            for (var cliente in $scope.tableData[codRelatorio.toString() + codDiaDaSemana.toString()]) {
                                $scope.tableData[codRelatorio.toString() + codDiaDaSemana.toString()][cliente].visitado = true;
                            }
                            return;
                        }
                        var visitasSemRealizacao = UtilsService.joinVisitaVendFornecedor(angular.copy(response.data), vendedorClienteFornecedor);
                        //console.log("ajuda:", $scope.tableData[codRelatorio.toString() + codDiaDaSemana.toString()])                        

                        //Caso contrário verifique quais foram visitados                        
                        for (var i in $scope.tableData[codRelatorio.toString() + codDiaDaSemana.toString()]) {
                            for (var j in visitasSemRealizacao) {
                                //Compara pelo codigo do cliente
                                if (i === j) {
                                    $scope.tableData[codRelatorio.toString() + codDiaDaSemana.toString()][i].visitado = false;
                                    break;
                                } else {
                                    $scope.tableData[codRelatorio.toString() + codDiaDaSemana.toString()][i].visitado = true;
                                }
                            }
                        }
                        //console.log("TABLE DATA:",$scope.tableData)
                    }, function errorCallback(response) {
                        console.log(response)
                    })
        });
    }

    $scope.finalizarRelatorio = function (relatorio) {
        relatorio.status = "Enviado para análise"
        ApiService.atualizaStatusRelatorio(relatorio)
                .then(function successCallback(response) {
                    ToastService.onSuccess("Relatório Finalizado com Sucesso!");
                }, function errorCallback(response) {
                    console.log(response)
                })

    }

    $scope.setDiaDaSemana = function (dia) {
        $scope.diaDaSemana = dia;
    }

    $scope.setRelatorio = function (relatorio) {
        $scope.relatorioCurrent = relatorio;
    }
});


