/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var myApp = angular.module('App');

myApp.controller('LoginCtrl', function ($scope, $rootScope, $cookies, AuthService, ToastService, ApiService) {
    $scope.user = {
        nome: "",
        senha: "",
    };

    console.log("LoginCtrl")

    function preencheZeros(str, tam) {
        if (str.lenght === 0) {
            return ""
        }        
        while (str.length < tam) {
            str = "0" + str;
        };        
        return str;
    }

    $scope.keypress = function (nome, $event) {
        //Enter ou Tab
        if ($event.keyCode === 13 || $event.keyCode === 9) {
            $event.preventDefault();
            $scope.user.nome = preencheZeros(nome, 6);
            document.getElementById("pass").focus();
        }
    }


    $rootScope.login = function () {
        var user = {};

        AuthService.login($scope.user)
                .then(function successCallback(response) {
                    user = {
                        codUsuario: response.data.nome.trim(),
                        role: response.data.role
                    };

                    ApiService.vendedores(user.codUsuario)
                            .then(function successCallback(response) {                                
                                //console.log("recebendo o nome do usuario")
                                //console.log(response.data)                                
                                var cookieExp = new Date();
                                cookieExp.setDate(cookieExp.getDate() + 1);            
                                
                                user.nomeUsuario = response.data.nome;                                
                                console.log('setando cookie:',user)
                                $cookies.putObject('user', user, {expires: cookieExp});
                                
                                window.location = '/';
                            }, function errorCallback(response) {
                                console.log("Erro ao tentar acessar tabela de funcionario");
                            })
                    
                    
                    //  window.location = '/';                    
                }, function errorCallback(response) {
                    ToastService.onError("Senha ou usuário incorreto!!!");
                });



    }
})


