/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var app = angular.module("App");

app.controller('CadastrarCtrl', function ($scope, $timeout, ApiService, $location, ToastService) {
    //$scope.client
    

    $scope.cadastroSuccess = false;
    $scope.esqueceuAlgumCampo = false;


    angular.extend($scope, {
        secoes: [],
        fornecedores: [],
        clientes: [],
        vendedores: [],
    })

    ApiService.Secoes()
            .then(function successCallback(response) {
                //   console.log("resposta: ", response);
                $scope.secoes = response.data;
            }, function errorCallback(response) {               
                console.log(response)
            })

    ApiService.Fornecedores()
            .then(function successCallback(response) {
                //    console.log("resposta: ", response);
                $scope.fornecedores = response.data;
            }, function errorCallback(response) {                
                console.log(response)
            })

    ApiService.Clientes()
            .then(function successCallback(response) {
                //  console.log("resposta:", response)
                $scope.clientes = response.data;
            }, function errorCallback(response) {
                console.log(response)
            })

    ApiService.Vendedores()
            .then(function successCallback(response) {                
                $scope.vendedores = response.data;
            }, function errorCallback(response) {               
                console.log(response)
            })

    $scope.cadastrar = function () {

        var dado = {
            "cliente": $scope.novo.cliente.id,
            "vendedor": $scope.novo.vendedor.id,
            "fornecedor": $scope.novo.fornecedor.id,
            "secao": $scope.novo.secao.id,
        }

        ApiService.InsereVendDaSecDoCli(dado)
                .then(function successCallback(response) {                                        
                    delete $scope.novo;                    
                    ToastService.onSuccess('Cadastro realizado com sucesso!');
                }, function errorCallback(response) {
                    ToastService.onError('Erro ao cadastrar!');
                    console.log(response.error);
                })


    }
})
