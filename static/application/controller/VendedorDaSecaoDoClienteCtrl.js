var app = angular.module("App")



app.controller('VendedorDaSecaoDoClienteCtrl', function ($scope, ApiService, $timeout, ToastService) {
    //Tem todos os, dps só aplica o filtro para ter os vendedores de determinado fornecedor
    
    var vendedores = {};
    
    $scope.toDelete = {}
    console.log("Estou no controller")
    $scope.selectVendedores = function(fornecedor){        
        ApiService.vendedoresDoFornecedor(fornecedor.id).then(
            function successCallback(response) {
                console.log("fazendo filtro:",response.data)
                $scope.vendedores = vendedores.filter(function(item){                    
                    for(var i = 0; i < response.data.length; i++){
                        if(item.id === response.data[i].cod_vendedor){                            
                            item.chaveEstrangeira = response.data[i].id;                            
                            return item;
                        }
                    }
                })
            }, function errorCallback(response){
                console.log("Deu ruim",response);
            });
    }
    
    
    function updateTela() {
        delete $scope.raiz        
        ApiService.vendedorDaSecDoCliente()
            .then(function successCallback(response) {                
                $timeout(function () {
                    $scope.raiz = getDistinctClientes(response.data);
                   // console.log("Raiz:", $scope.raiz);
                }, 0);

            }, function errorCallback(response) {
                console.log("ruim: ", response)
            });
    }
    $scope.cadastrar = function(dado){
        var dadoParaInserir = {
            chaveEstrangeira : dado.vendedor.chaveEstrangeira,
            cod_cliente      : dado.cliente.id,
            cod_secao        : dado.secao.id
        }
        ApiService.insereVendedorDaSecaoDoCliente(dadoParaInserir).then(
            function successCallback(response) {
               console.log("Sucesso")
               ToastService.onSuccess("Cadastro realizado com sucesso!!!");
               updateTela();
            }, function errorCallback(response){
                ToastService.onError("Erro ao cadastrar!!!");
                console.log("Deu ruim",response);
            });
        console.log("dado para cadastrar:",dadoParaInserir);
    }

    
    angular.extend($scope, {
        raiz: [],
        vendDaSecDoCli: [],
        clientes: [],
        fornecedores: [],
        secoes: [],
        vendedores: [],

        bufferAlterar: {
            cliente: {},
            fornecedor: {},
            secao: {},
            vendedor: {}
        },
        alterado: {
            cliente: {},
            fornecedor: {},
            secao: {},
            vendedor: {}
        },

        Ativar: function (list, item) {
            item.ativo = !item.ativo;
            for (var i in list) {
                if (list[i] == item) {
                    continue;
                }
                list[i].ativo = false;
            }
        },

        AlterarModal: function (idCli, nomeCli, idFor, nomeFor, idSec, nomeSec, idVend, nomeVend) {
            this.alterado.fornecedor = {
                nome: nomeFor,
                id: idFor
            }
            this.alterado.cliente = {
                nome: nomeCli,
                id: idCli
            }
            this.alterado.secao = {
                nome: nomeSec,
                id: idSec
            }
            this.alterado.vendedor = {
                nome: nomeVend,
                id: idVend
            }

            this.bufferAlterar.fornecedor = {
                nome: nomeFor,
                id: idFor
            }
            this.bufferAlterar.cliente = {
                nome: nomeCli,
                id: idCli
            }
            this.bufferAlterar.secao = {
                nome: nomeSec,
                id: idSec
            }
            this.bufferAlterar.vendedor = {
                nome: nomeVend,
                id: idVend
            }

        },

        update: function (dado, dadoUpdated) {
            ApiService.updateVendedorDaSecDoCliente(dado, dadoUpdated).then(
                    function successCallback(response) {
                        //Inserido com sucesso
                        ngToast.create({
                            className: 'success',
                            content: 'Item alterado com sucesso!'
                        });
                        updateTela();
                    }, function errorCallback(response) {
                ngToast.create({
                    className: 'danger',
                    content: 'Erro ao alterar:' + response.error + '!'
                });
            });

        },

        DeletarModal: function (cliId, fornId, secId, vendId) {
            $scope.toDelete = {
                cliente: cliId,
                fornecedor: fornId,
                secao: secId,
                vendedor: vendId
            }
            //console.log("oxe:" ,$scope.toDelete)
        },

        Delete: function (json) {
            console.log("vai deletar: ", json)
            ApiService.DeletaVendedorDaSecDoCliente(json).then(
                    function successCallback(response) {
                        ngToast.create({
                            className: 'success',
                            content: 'Item deletado com sucesso!'
                        });
                        updateTela();

                    }, function errorCallback(response) {
                ngToast.create({
                    className: 'danger',
                    content: 'Erro ao deletar:' + response.error + '!'
                });
            });
        }

    })

    function getDistinctClientes(json) {
        var clientes = {}
        , fornecedores = {}
        , secoes = {}
        , vendedores = {}
        , i = 0, item;
        /*        
         */
        function push(list, element) {
            list.indexOf(element) < 0 && list.push(element);
        }
        var fornecedor, secao, b1;
        while (item = json.shift()) {
            if (clientes[item.cliente] === undefined) {
                clientes[item.cliente] = {nomeCliente: item.nomeCliente, idCli: item.cliente, idTag: item.cliente.replace(/^\s+|\s+$/g, ""), fornecedores: [], ativo: false};
            }
            b1 = item.cliente + item.fornecedor;
            if (fornecedores[b1] === undefined) {
                fornecedores[b1] = {idFor: item.fornecedor, idTag: item.fornecedor.replace(/^\s+|\s+$/g, ""), nomeFor: item.nomeFornecedor, secoes: []};
            }
            fornecedor = fornecedores[b1];
            push(clientes[item.cliente].fornecedores, fornecedor);

            b1 += item.secao;
//            push(fornecedores[item.fornecedor].clientes, item.cliente);

            if (secoes[b1] === undefined) {

                secoes[b1] = {idSecao: item.secao, idTag: item.secao.replace(/^\s+|\s+$/g, ""), nomeSecao: item.nomeSecao, vendedores: []};
            }
            secao = secoes[b1];
            push(fornecedor.secoes, secao);
            //push(secoes[item.secao].fornecedores, item.fornecedor);

            if (vendedores[item.vendedor] === undefined) {
                vendedores[item.vendedor] = {idVendedor: item.vendedor, nomeVendedor: item.nomeVend};
            }
            push(secao.vendedores, vendedores[item.vendedor]);         
        }

        return {cli: clientes};
    }

     

    ApiService.vendedorDaSecDoCliente()
            .then(function successCallback(response) {
                //console.log("resposta do server:",response)
                $timeout(function () {
                    $scope.raiz = getDistinctClientes(response.data);
                    //console.log("Raiz:", $scope.raiz);
                }, 0);
            }, function errorCallback(response) {
                console.log("ruim: ", response)
            });
    
    ApiService.clientes()
            .then(function successCallback(response) {
                $scope.clientes = response.data;
                //console.log("clientes from server: ",response.data)
            }, function errorCallback(response) {
                console.log(response)
            })
    
    ApiService.fornecedores()
            .then(function successCallback(response) {            
                $scope.fornecedores = response.data;                
            }, function errorCallback(response) {
                console.log(response)
            });
            
    ApiService.vendedores()
            .then(function successCallback(response) {
                //    console.log("resposta:", response)               
                vendedores = response.data;            
            }, function errorCallback(response) {
                console.log(response)
            })        
    
    ApiService.secoes()
            .then(function successCallback(response) {
                //   console.log("resposta: ", response);
                $scope.secoes = response.data;
            }, function errorCallback(response) {
                console.log(response)
            })
})