/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var app = angular.module("App");

app.controller("PlanejamentoRelatorioCtrl", function ($scope,UtilsService, $q, ApiService, $rootScope, ToastService, DateService) {

    var user = $rootScope.usuario;

    $scope.tabs = [
        {title: 'Segunda'},
        {title: 'Terça'},
        {title: 'Quarta'},
        {title: 'Quinta'},
        {title: 'Sexta'}
    ];

    $scope.diaDaSemana = 1;


    $scope.headerTable;

    $scope.model = {
        name: 'Tabs'
    };

    $scope.tableData = {}
    $scope.tableDataRealizado = {}

    var vendedorClienteFornecedor;
    $scope.relatorioCurrent;
    $scope.clienteFornecedorDoVendedor;
    $scope.fornecedorDoVendedorFiltrado;
    $scope.objetivos;
    var fornecedoresDoVendedor;


    ApiService.getObjetivoVisita()
            .then(function successCallback(response) {
                $scope.objetivos = response.data;
            }, function errorCallback(response) {
                console.log("error: ", response)
            });

    ApiService.vendedorDaSecDoCliente()
            .then(function successCallback(response) {
                vendedorClienteFornecedor = response.data;

                var aux = []
                //console.log(response.data);
                $scope.clienteFornecedorDoVendedor = response.data.filter(function (obj) {
                    if (!aux.includes(obj.cliente) && user.codUsuario.trim() === obj.vendedor.trim()) {
                        aux.push(obj.cliente);
                        return obj;
                    }
                });

                aux = []
                //Pega todos os fornecedores do Vendedor, dps passa por uma filtragem
                fornecedoresDoVendedor = $scope.clienteFornecedorDoVendedor.filter(function (obj) {
                    if (!aux.includes(obj.fornecedor)) { //Equivalente ao contains do JAVA
                        aux.push(obj.fornecedor);
                        return obj;
                    }
                });

                //Seta o cabeçalho da tabela das metas do vendedor
                $scope.headerTable = [{cabecalho: 'Cliente'}];
                for (var x in fornecedoresDoVendedor) {
                    $scope.headerTable.push(fornecedoresDoVendedor[x]);
                }


                //console.log($scope.clienteFornecedorDoVendedor)
            }, function errorCallback(response) {
                console.log("error: ", response)
            });


    ApiService.getPlanejamentoDoVendedor($rootScope.usuario.codUsuario)
            .then(function successCallback(response) {
                $scope.planejamentos = response.data;
            }, function errorCallback(response) {
                console.log(response)
            })

    /**
     * Função que organiza os dados para mostrar na tabela
     * 
     * @param {type} visitas
     * @param {type} vend_forn
     * @returns {}
     */
    function organizaDados(visitas, vend_forn) {
        var clientes = {}
        , fornecedores = {}
        , i = 0, item;

        function push(list, element) {
            if (list[element.idFor] === undefined) {
                list[element.idFor.trim()] = element;
            }
        }
        var fornecedor, b1;

        while (item = visitas.shift()) {
            var cliente = vend_forn.find(function (element) {
                if (element.pk === item.pkCliFornVendSec) {
                    return true;
                }
            })
            //console.log("olha o item",item);
            if (clientes[cliente.cliente] === undefined) {
                clientes[cliente.cliente] = {nomeCliente: cliente.nomeCliente, diaDaSemana: item.diaDaSemana, idCli: cliente.cliente, objetivo: item.ObjVisita, fornecedores: {}};
            }
            b1 = cliente.cliente + cliente.fornecedor;
            if (fornecedores[b1] === undefined) {
                fornecedores[b1] = {idFor: cliente.fornecedor, codVisita: item.cod, nomeFor: cliente.nomeFornecedor, pk: item.pkCliFornVendSec, vlrVenda: item.vlrVenda};
            }
            fornecedor = fornecedores[b1];
            push(clientes[cliente.cliente].fornecedores, fornecedor);
        }
        return clientes;
    }

    $scope.finalizarPlanejamento = function (relatorio) {
        console.log("Finalize o planejamento do relatorio:", relatorio)
        relatorio.status = "Planejado";
        ApiService.atualizaStatusRelatorio(relatorio)
                .then(function successCallback(response) {
                    ToastService.onSuccess("Planejamento Finalizado com Sucesso!");
                }, function errorCallback(response) {
                    console.log(response)
                })
    }

    $scope.buscaPlanejamentoRealizado = function (codRelatorio, codDiaDaSemana) {
        console.log("chamando para o dia ", codDiaDaSemana)
        var d = $q.defer()
        ApiService.getVisitaRealizadaDoDiaFromRelatorio(codRelatorio, codDiaDaSemana)
                .then(function successCallback(response) {
                    d.resolve(response)
                    if (response.data.length !== 0)
                        $scope.tableDataRealizado[codRelatorio.toString() + codDiaDaSemana.toString()] = organizaDados(response.data, vendedorClienteFornecedor);
                }, function errorCallback(response) {
                    console.log(response)
                    d.reject()
                })
        return d.promise;
    }

    $scope.buscaPlanejamento = function (codRelatorio, codDiaDaSemana) {
        //Caso não tenha, processe   
        var d = $q.defer()
        ApiService.getPlanejamentoDoDia(codRelatorio, codDiaDaSemana)
                .then(function successCallback(response) {
                    if (response.data.length !== 0) {
                        $scope.tableData[codRelatorio.toString() + codDiaDaSemana.toString()] = organizaDados(angular.copy(response.data), vendedorClienteFornecedor);
                        //console.log("RESULTADO:",$scope.tableData[codRelatorio+codDiaDaSemana]);
                    }
                    d.resolve(response)
                }, function errorCallback(response) {
                    console.log(response)
                    d.reject()
                })
        return d.promise;
    }
    
   
    $scope.gerarRelatorio = function (codRelatorio) {

        ApiService.getAllVisitaFromRelatorio(codRelatorio)
                .then(function sucessCallback(response) {
                    //console.log("RESPOSTA DO SERVER:",response.data)
                    var visitasPlanejadas = UtilsService.getAllVisitasPlanejadas(response.data,vendedorClienteFornecedor,codRelatorio);
                    var visitasRealizadas = UtilsService.getAllVisitasRealizada(response.data,vendedorClienteFornecedor,codRelatorio);
                
                    var result = [];
                    // Monta Planejado
                    for (var i in visitasPlanejadas) {
                        //console.log('olha o i', i)
                        var value = visitasPlanejadas[i];
                        var realizado = visitasRealizadas[i];
                        var array = [];
                        for (var x in value) { //Pega cada cliente
                            //console.log("ESTOU ITETANDO NO CLIENTE: ", value[x].nomeCliente)
                            var plan = {};
                            var real = {};

                            plan.nome = value[x].nomeCliente;
                            real.nome = realizado[x].nomeCliente;

                            plan.objetivo = UtilsService.getObjetivoToString($scope.objetivos,value[x].objetivo).trim();
                            real.objetivo = UtilsService.getObjetivoToString($scope.objetivos,realizado[x].objetivo).trim();

                            var total = 0;
                            var totalReal = 0;
                            for (var k in value[x].fornecedores) {
                                total += value[x].fornecedores[k].vlrVenda;
                                totalReal += realizado[x].fornecedores[k].vlrVenda;
                            }
                            plan.vendas = total;
                            real.vendas = totalReal;
                            array.push({plan: plan, real: real});
                            //console.log('diga x',value[x])
                        }

                        result[i.charAt(i.length - 1)] = array;
                        //console.log("ESSE EH O DIA ", i.charAt(i.length - 1))
                    }
                    
                    //console.log('array:', array)
                    //console.log('RESULTADO:', result);                    
                    ApiService.gerarRelatorio({visitas:result});                    
                }, function errorCallback() {
                    console.log("DEU ERRo")
                })

    }

    $scope.setCliente = function (cliente, objetivos) {
        $scope.alterado = angular.copy(cliente);
        $scope.alterado.objObjetivo = objetivos.find(function (element) {
            return element.codObjetivo === $scope.alterado.objetivo;
        })
        //console.log('obj alterado',$scope.alterado)
    }

    $scope.alterarVisitaPlanejada = function (visitaAlterada, relatorio) {
        var data = new Date();
        for (var x in visitaAlterada.fornecedores) {
            var temp = {
                cod: visitaAlterada.fornecedores[x].codVisita,
                diaDaSemana: visitaAlterada.diaDaSemana,
                ObjVisita: visitaAlterada.objetivo,
                codRelatorio: relatorio.Cod_relatorio,
                pkCliFornVendSec: visitaAlterada.fornecedores[x].pk,
                vlrVenda: parseFloat(visitaAlterada.fornecedores[x].vlrVenda.replace(/,/g, '.')),
                tipo: 'P',
                dataRegistro: DateService.dateToTimeStamp(data)
            }
            ApiService.updateVisita(temp)
                    .then(function successCallback(response) {
                        ToastService.onSuccess("Planejamento de Visita alterado com sucesso!!!")
                        $scope.buscaPlanejamento(temp.codRelatorio, temp.diaDaSemana, true);
                    }, function errorCallback(response) {
                        ToastService.onError("Erro ao alterar planejamento da visita")
                    })
        }

    }

    $scope.filtraFornecedor = function (cliente) {
        cliente.arrayPk = [];

        //cliente.cliente é o id direto da resposta do servidor e o idCli é o objeto que eu gero na função que organiza os dados
        var id = cliente.cliente || cliente.idCli;

        $scope.fornecedorDoVendedorFiltrado = vendedorClienteFornecedor.filter(function (obj) {
            if (obj.cliente === id && user.codUsuario.trim() === obj.vendedor.trim()) {
                cliente.arrayPk.push(obj.pk);
                return obj;
            }
        });
    }



    $scope.cadastrarVisita = function (objetivoVisita, relatorio, diaDaSemana, cliente, valorFornecedor) {
        var data = new Date();
        var i = 0;
        //console.log("relatorio:", relatorio);
        for (var x in valorFornecedor) {
            var temp = {
                diaDaSemana: diaDaSemana,
                ObjVisita: objetivoVisita.codObjetivo,
                codRelatorio: relatorio.Cod_relatorio,
                pkCliFornVendSec: cliente.arrayPk[i],
                vlrVenda: parseFloat(valorFornecedor[x].replace(/,/g, '.')), //Troca , por .
                tipo: 'P',
                dataRegistro: DateService.dateToTimeStamp(data)
            };
            i++;


            ApiService.insereVisitaPlanejadaNoRelatorio(temp)
                    .then(function successCallback(response) {
                        //Atualiza a tela buscando novamento o planejamento daquele dia
                        $scope.buscaPlanejamento(temp.codRelatorio, temp.diaDaSemana, true);
                        ToastService.onSuccess("Visita Cadastrada Com sucesso!")

                    }, function errorCallback(response) {
                        ToastService.onError("Erro ao cadastrar visita!!!")
                        console.log("estou mandando:", temp)
                        console.log(response);
                    })
        }
        delete $scope.novo;

    }

    $scope.setDiaDaSemana = function (dia) {
        //console.log("setando:", dia)
        $scope.diaDaSemana = dia;
    }

    $scope.setRelatorio = function (relatorio) {
        //console.log("setando:",relatorio)        
        $scope.relatorioCurrent = relatorio;
    }

    $scope.cadastrarPlanejamento = function (dtInicio, dtFinal) {
        //console.log("Data inicio:",dtInicio," : ",dtInicio.getTime());
        //FORMATA PARA O FORMATO TIMESTAMP DO FIREBIRD
        var auxDt_inicio = [("0" + dtInicio.getDate()).slice(-2), ("0" + (dtInicio.getMonth() + 1)).slice(-2), dtInicio.getFullYear() % 100].join(".") + " 10:00";

        var auxDt_fim = [("0" + dtFinal.getDate()).slice(-2), ("0" + (dtFinal.getMonth() + 1)).slice(-2), dtFinal.getFullYear() % 100].join(".") + " 10:00";

        var obj = {
            cod_vendedor: user.codUsuario,
            dt_inicio: auxDt_inicio,
            dt_final: auxDt_fim,
            status: "Cadastrado"
        }
        console.log("obj", obj);

        ApiService.inserePlanejamentoDoRelatorio(obj)
                .then(function successCallback(response) {
                    ToastService.onSuccess("Planejamento Cadastrado Com sucesso!!!");
                    $scope.planejamentos.push({
                        cod_vendedor: user.codUsuario,
                        dt_inicio: dtInicio,
                        dt_final: dtFinal,
                        status: "Cadastrado"
                    });
                    ApiService.getPlanejamentoDoVendedor($rootScope.usuario.codUsuario)
                            .then(function successCallback(response) {
                                $scope.planejamentos = response.data;
                            }, function errorCallback(response) {
                                console.log(response)
                            })

                }, function errorCallback(response) {
                    ToastService.onSuccess("Erro ao Cadastrar Planejamento!!!");
                })
    }

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: false
    };

    $scope.dateOptionsInit = {
        dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    $scope.dateOptionsFim = {
        dateDisabled: disabledFim,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    // Disable weekend selection
    function disabledFim(data) {
        var date = data.date,
                mode = data.mode;
        return mode === 'day' && (date.getDay() !== 6); //habilita somente sábado para seleção
    }

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
                mode = data.mode;
        return mode === 'day' && (date.getDay() !== 1); //habilita somente segunda
    }

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        // $scope.dateOptionsInit.minDate = $scope.inlineOptions.minDate;
    }();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };


    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    function getDayClass(data) {
        var date = data.date,
                mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }
});


