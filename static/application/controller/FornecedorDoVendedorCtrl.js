/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var app = angular.module("App");

app.controller('FornecedorDoVendedorCtrl', function ($scope, ApiService, ToastService) {
    //console.log("ctrl, registrado");

    ApiService.vendedores()
            .then(function successCallback(response) {
                $scope.vendedores = response.data;
            }, function errorCallback(response) {
                console.log(response)
            })

    ApiService.fornecedores()
            .then(function successCallback(response) {
                //    console.log("resposta: ", response);
                $scope.fornecedores = response.data;
            }, function errorCallback(response) {
                console.log(response)
            })

    $scope.cadastrar = function (dado) {
        var objToSave = {
            cod_vendedor: dado.vendedor.id,
            cod_fornecedor: dado.fornecedor.id,
            flag: dado.flag
        };

        ApiService.insereVendedorFornecedor(objToSave)
                .then(function successCallback(response) {
                    ToastService.onSuccess("Cadastro realizado com sucesso!!!");
                }, function errorCallback(response) {
                    ToastService.onError("Erro ao cadastrar!!!");
                });        
    };
});
