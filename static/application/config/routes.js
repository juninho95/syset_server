/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module("App")
        .config(function ($routeProvider, $locationProvider, ngToastProvider) {

            //var base = "/localhost:8080/syset_cliente/";
            $routeProvider
                    .when("/", {
                        templateUrl: 'static/application/views/home.html'
                                // controller: 'HomeCtrl'
                    })

                    .when("/fornecedorDoVendedor", {
                        templateUrl: 'static/application/views/fornecedorDoVendedor.html',
                        controller: 'FornecedorDoVendedorCtrl'
                    })
                    .when("/vendedorDaSecaoDoCliente", {
                        templateUrl: 'static/application/views/vendedorDaSecaoDoCliente.html',
                        controller: 'VendedorDaSecaoDoClienteCtrl'
                    })
                    .when("/auth", {
//                        redirectTo:
//                        templateUrl: 'static/application/views/listarVendDaSecDoCli.html',
//                        controller: 'ListarCtrl'
                    })
                    .when('/usuario', {
                        templateUrl: 'static/application/views/usuario.html',
                        controller: 'UsuarioCtrl'
                    })

                    .when("/planejamentoRelatorio", {
                        templateUrl: 'static/application/views/planejamentoRelatorio.html',
                        controller: 'PlanejamentoRelatorioCtrl'

                    })
                    .when("/rotaRealizada", {   
                        templateUrl: 'static/application/views/rotaRealizada.html',
                        controller: 'RotaRealizadaCtrl'
                    })

                    .otherwise({redirect: '/'});

            $locationProvider.hashPrefix('');

            ngToastProvider.configure({
                animation: 'slide' // or 'fade'
            });

            //$locationProvider.html5Mode(true);
        })
