/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var app = angular.module("App")

app.filter('orderObjectBy', function () {
    return function (items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function (item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            return (a[field].trim() > b[field].trim() ? 1 : -1);
        });
        if (reverse)
            filtered.reverse();
        return filtered;
    };
})


app.filter('regex', function() {
  return function(input, field, regex,dado) {
      
      if(dado !== undefined)
        dado = dado.toLowerCase();
    
      var patt = new RegExp(regex+dado);      
      var out = [];
      for (var i = 0; i < input.length; i++){
          if(patt.test(input[i][field].toLowerCase()))
              out.push(input[i]);
      }      
    if(out.length === 0){
        return input;
    }
    return out;
  };
});
