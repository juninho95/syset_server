package main

import(
	//Pacotes padrão ou Middlewares
	"log"
	"fmt"
	"gopkg.in/kataras/iris.v6"	
	"gopkg.in/kataras/iris.v6/adaptors/gorillamux"
	"database/sql"	
	_ "github.com/nakagami/firebirdsql"
	//Meus pacotes
	"syset_server/controllers"
	"syset_server/auth"	
)       



func main() {

        dbET, err := sql.Open("firebirdsql", "SYSDBA:masterkey@192.168.0.11:3050/c:\\Syspdv\\syspdv_srv.fdb")
	dbET_vendas, err2 := sql.Open("firebirdsql", "SYSDBA:masterkey@192.168.0.11:3050/c:\\SysET\\ETVENDAS.FDB")        

        defer dbET.Close()

	if err != nil { log.Fatal(err) }
	if err2 != nil{ log.Fatal(err)	}

	app := iris.New();
	app.Adapt(gorillamux.New())
	app.Adapt(iris.DevLogger())

        app.UseFunc(func(ctx *iris.Context){		
            ctx.Set("db", dbET)		
            ctx.Set("db_vendas",dbET_vendas)		
            ctx.Next()
	})

	autenticacaoMiddleWare := auth.AuthSysET{}
	autenticacaoMiddleWare.Register(app)	
        //Se nao encontra a url requisitada redireciona para logi        
   
        app.OnError(iris.StatusNotFound, func(ctx *iris.Context) {
            //fmt.Println("Deu ERRO, NOTFOUND");
            //ctx.Redirect("/auth")
            //ctx.Log("http status: 404 happened!")
        })

	app.Get("/static/{file:[\\/|\\w|\\.|-]*}", func(ctx *iris.Context) {				
            ctx.ServeFile("static/" + ctx.Param("file"), false)		
        })
        
        app.Delete("/api/authentication/destroy", func(ctx *iris.Context){            
            fmt.Println("destruindo a sessao");
            ctx.Session().Clear()
            ctx.SessionDestroy()
            //ctx.Redirect("/auth")
        })
        
        app.Get("/api/authentication/check", autenticacaoMiddleWare.Serve);
        
        app.Post("/api/authentication",func(ctx *iris.Context){
            err := autenticacaoMiddleWare.Login(ctx)
            if err != nil {fmt.Println("Deu erro:",err)}
        })
        
	app.Get("/", func(ctx *iris.Context){
            if logado,_ := ctx.Session().GetBoolean("logado"); !logado {              
                ctx.Redirect("/auth")
                return;
            }            
            ctx.ServeFile("static/index.html", false)
	})
        
        app.Get("/teste",func(ctx *iris.Context){
            ctx.ServeFile("templateRelatorio.html",false)
        })
        
        app.Get("/auth", func(ctx *iris.Context){            
            ctx.ServeFile("static/application/views/login.html", false)
	})

	
        
        //Habilita autenticação para acesso da API REST
        //app.UseFunc(autenticacaoMiddleWare.Serve);

        //------------------------------ Registro de Controllers --------------------------------------
	secoes := controllers.SecoesCtrl{}
	secoes.Register(app)

	clientes := controllers.ClientesCtrl{}
	clientes.Register(app)

	fornecedores := controllers.FornecedoresCtrl{}
	fornecedores.Register(app)
	
	vendedores := controllers.VendedoresCtrl{}
	vendedores.Register(app)
        
        vendedorFornecedor := controllers.VendedorFornecedorCtrl{}
        vendedorFornecedor.Register(app)

	vendedorDaSecDoCliCtrl := controllers.VendedorDaSecDoCliCtrl{}
	vendedorDaSecDoCliCtrl.Register(app)
        
        vendedorDaSecaoDoClienteCtrl := controllers.VendedorDaSecaoDoClienteCtrl{}
        vendedorDaSecaoDoClienteCtrl.Register(app)
        
        relatorioCtrl := controllers.RelatorioDeVendaCtrl{}
        relatorioCtrl.Register(app)

        geraPDFCtrl := controllers.GeraPdfCtrl{}
        geraPDFCtrl.Register(app)
	app.Listen(":8080")
}